# Simplified Chinese translation of gyrus
# Copyright (C) 2005, 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the gyrus package.
# Funda Wang <fundawang@linux.net.cn>, 2005
# du baodao <centerpoint@139.com>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: gyrus master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gyrus&component=general\n"
"POT-Creation-Date: 2010-06-16 19:00+0000\n"
"PO-Revision-Date: 2010-07-31 12:10+0800\n"
"Last-Translator: du baodao <centerpoint@139.com>\n"
"Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../gyrus.desktop.in.in.h:1
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr "您的 Cyrus IMAP 服务器的邮箱管理员"

#: ../gyrus.desktop.in.in.h:2
msgid "Gyrus IMAP Cyrus Administrator"
msgstr "Cyrus IMAP 管理员"

#: ../src/glade/create_mailbox.glade.h:1
msgid "*"
msgstr "*"

#: ../src/glade/create_mailbox.glade.h:2
msgid "<b>Name:</b>"
msgstr "<b>名称：</b>"

#: ../src/glade/create_mailbox.glade.h:3
msgid "<b>Quota (MB):</b>"
msgstr "<b>配额(MB)：</b>"

#: ../src/glade/create_mailbox.glade.h:4
msgid "Assign quota"
msgstr "指派配额"

#: ../src/glade/create_mailbox.glade.h:5
msgid "Create mailbox"
msgstr "创建邮箱"

#: ../src/glade/find.glade.h:1
msgid "Find"
msgstr "查找"

#: ../src/glade/find.glade.h:2
msgid "Match _entire word only"
msgstr "匹配整个单词(_E)"

#: ../src/glade/find.glade.h:3
msgid "Search for:"
msgstr "搜索："

#: ../src/glade/find.glade.h:4
msgid "_Wrap around"
msgstr ""

#: ../src/glade/page.glade.h:1
msgid "<b>Assigned space:</b>"
msgstr "<b>指派的空间：</b>"

#: ../src/glade/page.glade.h:2
msgid "<b>Enter your password</b>"
msgstr "<b>输入您的密码</b>"

#: ../src/glade/page.glade.h:3
msgid "<b>Free space:</b>"
msgstr "<b>剩余空间：</b>"

#: ../src/glade/page.glade.h:4
msgid "<b>Host:</b>"
msgstr "<b>主机：</b>"

#: ../src/glade/page.glade.h:5
msgid "<b>Owner:</b>"
msgstr "<b>所有者：</b>"

#: ../src/glade/page.glade.h:6
msgid "<b>Port:</b>"
msgstr "<b>端口：</b>"

#: ../src/glade/page.glade.h:7
msgid "<b>User:</b>"
msgstr "<b>用户：</b>"

#: ../src/glade/page.glade.h:8
msgid "Access control list"
msgstr "访问控制列表"

#: ../src/glade/page.glade.h:9
msgid "Modify quota"
msgstr "修改配额"

#: ../src/glade/page.glade.h:10
msgid "New quota (MB)"
msgstr "新配额(MB)"

#: ../src/glade/page.glade.h:11
msgid "Password"
msgstr "密码"

#: ../src/glade/page.glade.h:12 ../src/gyrus-admin.c:825
#: ../tests/gyrus-talk.glade.h:7
msgid "_Connect"
msgstr "连接(_C)"

#: ../src/glade/preferences.glade.h:1
msgid "<b>Mailbox quota:</b>"
msgstr "<b>邮箱配额：</b>"

#: ../src/glade/preferences.glade.h:2
msgid "<b>Mailboxes tree:</b>"
msgstr "<b>邮箱树：</b>"

#: ../src/glade/preferences.glade.h:3
msgid "Default suffix for changing quota:"
msgstr "更改配额的默认后缀："

#: ../src/glade/preferences.glade.h:4
msgid "Preferences"
msgstr "首选项"

#: ../src/glade/preferences.glade.h:5
msgid "View complete mailboxes tree"
msgstr "查看完整的邮箱树"

#: ../src/glade/report.glade.h:2
#, no-c-format
msgid "Over (%)"
msgstr "超过(%d)"

#: ../src/glade/report.glade.h:3
msgid "Report"
msgstr "报告"

#. Translate only Autodetect please.
#: ../src/glade/sessions.glade.h:2
msgid ""
".\n"
"/\n"
"Autodetect"
msgstr ""
".\n"
"/\n"
"自动检测"

#: ../src/glade/sessions.glade.h:5
msgid "<b>Options</b>"
msgstr "<b>选项</b>"

#: ../src/glade/sessions.glade.h:6
msgid "<b>Session details</b>"
msgstr "<b>会话细节</b>"

#: ../src/glade/sessions.glade.h:7 ../tests/gyrus-talk.glade.h:3
msgid "Host:"
msgstr "主机："

#: ../src/glade/sessions.glade.h:8
msgid "Mailbox hierarchy separator:"
msgstr "邮箱等级分隔符："

#: ../src/glade/sessions.glade.h:9
msgid "Open session"
msgstr "打开会话"

#: ../src/glade/sessions.glade.h:10
msgid "Password:"
msgstr "密码："

#: ../src/glade/sessions.glade.h:11 ../tests/gyrus-talk.glade.h:4
msgid "Port:"
msgstr "端口："

#: ../src/glade/sessions.glade.h:12
msgid "Session name:"
msgstr "会话名称："

#: ../src/glade/sessions.glade.h:13 ../tests/gyrus-talk.glade.h:6
msgid "Use a secure connection"
msgstr "使用安全连接"

#: ../src/glade/sessions.glade.h:14
msgid "Username:"
msgstr "用户名："

#: ../src/gyrus-admin-acl.c:54 ../src/gyrus-admin-acl.c:103
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr "邮箱“%s”不存在。"

#: ../src/gyrus-admin-acl.c:76
msgid "Invalid identifier."
msgstr "无效的标识符。"

#: ../src/gyrus-admin-acl.c:82
msgid "Empty entry name."
msgstr "清空输入的名称。"

#: ../src/gyrus-admin-acl.c:87
msgid "Empty mailbox name."
msgstr "清空邮箱名称。"

#: ../src/gyrus-admin-acl.c:107
msgid "Missing required argument to Setacl"
msgstr "Setacl 缺少所需的参数"

#: ../src/gyrus-admin-acl.c:143 ../src/gyrus-admin-mailbox.c:82
msgid "Permission denied"
msgstr "权限被禁止"

#: ../src/gyrus-admin-acl.c:175
msgid "Empty access control list."
msgstr "清空访问控制列表。"

#: ../src/gyrus-admin-mailbox.c:80
msgid "Quota does not exist"
msgstr "配额不存在"

#: ../src/gyrus-admin-mailbox.c:174
#, c-format
msgid "Quota overloaded"
msgstr "配额超载"

#: ../src/gyrus-admin-mailbox.c:232
msgid "Quota not valid. Please try again."
msgstr "配额无效。请重试"

#: ../src/gyrus-admin-mailbox.c:246
msgid ""
"Unable to change quota. Are you sure do you have the appropriate permissions?"
msgstr "无法改变配额。您确定您有合适的权限？"

#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr "“%s”不是有效的邮箱名称。请尝试不同的名称。"

#: ../src/gyrus-admin-mailbox.c:340
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr "父邮箱“%s”不存在。请刷新邮箱列表并重试。"

#: ../src/gyrus-admin-mailbox.c:350
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr "邮箱“%s”已存在。请试用不同的名称。"

#: ../src/gyrus-admin-mailbox.c:363
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropriate "
"permissions?"
msgstr "无法创建邮箱。您确定您有合适的权限吗？"

#: ../src/gyrus-admin-mailbox.c:373
msgid "Mailbox created, but could not set quota."
msgstr "邮箱已创建，但无法设定配额。"

#: ../src/gyrus-admin-mailbox.c:437
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr "无法删除“%s”。权限禁止。"

#: ../src/gyrus-admin-mailbox.c:638
msgid "new entry"
msgstr "新建项"

#: ../src/gyrus-admin.c:468 ../src/gyrus-report.c:288
#, c-format
msgid "Users (%d)"
msgstr "用户数(%d)"

#: ../src/gyrus-admin.c:474
#, c-format
msgid "Orphaned mailboxes (%d)"
msgstr "孤儿邮箱(%d)"

#: ../src/gyrus-admin.c:518 ../src/gyrus-admin.c:834
#, fuzzy
#| msgid "Create mailbox"
msgid "Orphaned mailboxes"
msgstr "创建邮箱"

#: ../src/gyrus-admin.c:518 ../src/gyrus-admin.c:832 ../src/gyrus-report.c:169
msgid "Users"
msgstr "用户数"

#: ../src/gyrus-admin.c:586
msgid "lookup"
msgstr "查阅"

#: ../src/gyrus-admin.c:587
msgid "read"
msgstr "读取"

#: ../src/gyrus-admin.c:588
msgid "seen"
msgstr "可见"

#: ../src/gyrus-admin.c:589
msgid "write"
msgstr "写入"

#: ../src/gyrus-admin.c:590
msgid "insert"
msgstr "插入"

#: ../src/gyrus-admin.c:591
msgid "post"
msgstr "发表"

#: ../src/gyrus-admin.c:592
msgid "create"
msgstr "创建"

#: ../src/gyrus-admin.c:593
msgid "delete"
msgstr "删除"

#: ../src/gyrus-admin.c:594
msgid "admin"
msgstr "管理"

#: ../src/gyrus-admin.c:602
msgid "Identifier"
msgstr "标识符"

#: ../src/gyrus-admin.c:672
#, c-format
msgid "%s could not be found. Please check the name and try again."
msgstr "找不到 %s。请检查名称后再试一次。"

#: ../src/gyrus-admin.c:683
#, c-format
msgid "Could not connect to %s, port %d."
msgstr "无法连接到 %s 端口 %d。"

#: ../src/gyrus-admin.c:982
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr "无法使用空密码进行连接。请输入您的密码。"

#: ../src/gyrus-admin.c:989
msgid "Incorrect login/password"
msgstr "登录名/密码不正确"

#: ../src/gyrus-admin.c:997
msgid "_Disconnect"
msgstr "断开连接(_D)"

#: ../src/gyrus-admin.c:1350
msgid "Could not change permission. Server error: "
msgstr "无法更改权限。服务器错误："

#: ../src/gyrus-admin.c:1393
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr "已经存在名为“%s”的项。覆盖吗？"

#: ../src/gyrus-dialog-find-mailbox.c:172
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr "在邮箱列表中没有发现“%s”"

#: ../src/gyrus-dialog-find-mailbox.c:275
msgid "Find mailbox"
msgstr "查找邮箱"

#: ../src/gyrus-dialog-mailbox-new.c:94
msgid "Quota not valid"
msgstr "配额无效"

#: ../src/gyrus-dialog-mailbox-new.c:216
msgid "New mailbox"
msgstr "新建邮箱"

#: ../src/gyrus-main-app.c:148
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr "确定要删除邮箱 “%s” 和它的子邮箱吗？"

#: ../src/gyrus-main-app.c:270 ../src/gyrus-main-app.c:449
#: ../src/gyrus-main-app.c:745
msgid "Cyrus IMAP Administrator"
msgstr "Cyrus IMAP 管理员"

#: ../src/gyrus-main-app.c:284
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr "%s - Cyrus IMAP 管理员"

#: ../src/gyrus-main-app.c:394
msgid "_File"
msgstr "文件(_F)"

#: ../src/gyrus-main-app.c:395
msgid "_Edit"
msgstr "编辑(_E)"

#: ../src/gyrus-main-app.c:396
msgid "_ACL"
msgstr "_ACL"

#: ../src/gyrus-main-app.c:397
msgid "_View"
msgstr "查看(_V)"

#: ../src/gyrus-main-app.c:398
msgid "_Help"
msgstr "帮助(_H)"

#: ../src/gyrus-main-app.c:399
msgid "Go to server..."
msgstr "转到服务器..."

#: ../src/gyrus-main-app.c:400
msgid "Show the list of servers"
msgstr "显示服务器列表"

#: ../src/gyrus-main-app.c:410
msgid "Add mailbox"
msgstr "添加邮箱"

#: ../src/gyrus-main-app.c:411
msgid "Add a mailbox under the one selected"
msgstr "在选择的邮箱下创建新邮箱"

#: ../src/gyrus-main-app.c:413
msgid "Search for a mailbox in current server"
msgstr "在当前服务器中搜索邮箱"

#: ../src/gyrus-main-app.c:415
msgid "Refresh the mailbox list"
msgstr "刷新邮箱列表"

#: ../src/gyrus-main-app.c:416
msgid "Create report..."
msgstr "创建报告..."

#: ../src/gyrus-main-app.c:417
msgid "Create report of users with quota problems"
msgstr "创建有配额问题的用户的报告"

#: ../src/gyrus-main-app.c:421
msgid "New entry"
msgstr "新建项"

#: ../src/gyrus-main-app.c:422
msgid "Create a new ACL entry in current mailbox"
msgstr "在当前邮箱内创建新 ACL 项"

#: ../src/gyrus-main-app.c:423
msgid "Remove mailbox"
msgstr "删除邮箱"

#: ../src/gyrus-main-app.c:424
msgid "Remove current mailbox from the server"
msgstr "从服务器删除当前邮箱"

#: ../src/gyrus-main-app.c:429
msgid "Rename entry"
msgstr "重命名项"

#: ../src/gyrus-main-app.c:430
msgid "Rename selected ACL entry"
msgstr "重命名选定的 ACL 项"

#: ../src/gyrus-main-app.c:431
msgid "Delete entry"
msgstr "删除项"

#: ../src/gyrus-main-app.c:432
msgid "Delete selected ACL entry"
msgstr "删除选定的 ACL 条目"

#: ../src/gyrus-main-app.c:565
msgid "translators-credits"
msgstr "翻译者-致谢"

#: ../src/gyrus-main-app.c:575
msgid "GNOME Cyrus Administrator"
msgstr "GNOME Cyrus 管理员"

#: ../src/gyrus-main-app.c:577
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""
"(c) 2003-2005 GNOME 基金会\n"
"(c) 2004-2005 Claudio Saavedra"

#: ../src/gyrus-main-app.c:579
msgid "Administration tool for Cyrus IMAP servers."
msgstr "Cyrus IMAP 服务器的管理工具。"

#. set title
#: ../src/gyrus-report.c:106
#, c-format
msgid "Mailbox space usage report for %s"
msgstr "%s 的邮箱空间使用报告"

#: ../src/gyrus-report.c:190 ../src/gyrus-report.c:490
msgid "Quota (%)"
msgstr "配额(%)"

#: ../src/gyrus-report.c:203 ../src/gyrus-report.c:494
msgid "Assigned (KB)"
msgstr "分配的(KB)"

#: ../src/gyrus-report.c:214 ../src/gyrus-report.c:498
msgid "Used (KB)"
msgstr "已使用(KB)"

#. Translators: this represents the number of pages being printed.
#: ../src/gyrus-report.c:463
#, c-format
msgid "%d/%d"
msgstr "%d/%d"

#: ../src/gyrus-report.c:486
msgid "User"
msgstr "用户"

#: ../src/gyrus-session.c:164
msgid "Edit session"
msgstr "编辑会话"

#: ../src/gyrus-session.c:178
msgid "New session"
msgstr "新建会话"

#: ../src/gyrus-session.c:357
msgid "A session name is required."
msgstr "需要会话名称。"

#: ../src/gyrus-session.c:368
#, c-format
msgid "Session named \"%s\" already exists."
msgstr "已经存在名为“%s”的会话。"

#: ../src/gyrus-session.c:465
msgid "Autodetect"
msgstr "自动检测"

#: ../src/gyrus-session.c:550
msgid "Session"
msgstr "会话"

#: ../src/gyrus-session.c:702
msgid "No host specified."
msgstr "未指定主机。"

#: ../tests/gyrus-talk.glade.h:1
msgid "<b>Connection</b>"
msgstr "<b>连接</b>"

#: ../tests/gyrus-talk.glade.h:2
msgid "Command:"
msgstr "命令："

#: ../tests/gyrus-talk.glade.h:5
msgid "Talk - Echo client"
msgstr ""

#: ../tests/gyrus-talk.glade.h:8
msgid "_Send"
msgstr "发送(_S)"

#~ msgid "Host (or IP address)"
#~ msgstr "主机(或 IP 地址)"

#~ msgid "Save session as"
#~ msgstr "会话另存为"

#~ msgid "(c) 2003 GNOME Foundation"
#~ msgstr "(c) 2003 GNOME 基金会"

#~ msgid ""
#~ "B\n"
#~ "KB\n"
#~ "MB\n"
#~ "GB\n"
#~ "TB"
#~ msgstr ""
#~ "B\n"
#~ "KB\n"
#~ "MB\n"
#~ "GB\n"
#~ "TB"

#~ msgid "Could not change quota."
#~ msgstr "无法更改配额。"

#~ msgid "Mailbox '%s' does not exists."
#~ msgstr "邮箱“%s”不存在。"
