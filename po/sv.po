# Swedish messages for gyrus.
# Copyright © 2005, 2006, 2008, 2010, 2015 Free Software Foundation, Inc.
# Christian Rose <menthos@menthos.com>, 2005.
# Daniel Nylander <po@danielnylander.se>, 2006, 2008, 2010.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2015.
#
#
msgid ""
msgstr ""
"Project-Id-Version: gyrus\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gyrus&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-12-19 14:26+0000\n"
"PO-Revision-Date: 2015-05-07 10:59+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.6\n"

#: ../gyrus.desktop.in.in.h:1
msgid "Gyrus IMAP Cyrus Administrator"
msgstr "Gyrus IMAP Cyrus-administratör"

#: ../gyrus.desktop.in.in.h:2
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr "Administrera postlådorna för dina IMAP Cyrus-servrar"

#: ../src/ui/create_mailbox.xml.h:1
msgid "Name:"
msgstr "Namn:"

#: ../src/ui/create_mailbox.xml.h:2
msgid "Quota (MB):"
msgstr "Kvot (MB):"

#: ../src/ui/create_mailbox.xml.h:3
msgid "Assign quota"
msgstr "Tilldela kvot"

#: ../src/ui/find.xml.h:1
msgid "Search for:"
msgstr "Sök efter:"

#: ../src/ui/find.xml.h:2
msgid "Match _entire word only"
msgstr "Matcha _endast hela ord"

#: ../src/ui/find.xml.h:3
msgid "_Wrap around"
msgstr "_Börja om från början"

#: ../src/ui/page.xml.h:1
msgid "Free space:"
msgstr "Ledigt utrymme:"

#: ../src/ui/page.xml.h:2
msgid "Assigned space:"
msgstr "Tilldelat utrymme:"

#: ../src/ui/page.xml.h:3
msgid "Owner:"
msgstr "Ägare:"

#: ../src/ui/page.xml.h:4
msgid "New quota (MB)"
msgstr "Ny kvot (MB)"

#: ../src/ui/page.xml.h:5
msgid "Modify quota"
msgstr "Ändra kvot"

#: ../src/ui/page.xml.h:6
msgid "Access control list"
msgstr "Åtkomstkontrollista"

#: ../src/ui/page.xml.h:7 ../src/ui/sessions_edit.xml.h:5
#: ../tests/gyrus-talk.xml.h:2
msgid "Host:"
msgstr "Värd:"

#: ../src/ui/page.xml.h:8
msgid "User:"
msgstr "Användare:"

#: ../src/ui/page.xml.h:9 ../src/ui/sessions_edit.xml.h:4
#: ../tests/gyrus-talk.xml.h:4
msgid "Port:"
msgstr "Port:"

#: ../src/ui/password.xml.h:1
msgid "Password"
msgstr "Lösenord"

#: ../src/ui/password.xml.h:2
msgid "Enter your password"
msgstr "Ange ditt lösenord"

#: ../src/ui/report.xml.h:1
msgid "Report"
msgstr "Rapport"

#: ../src/ui/report.xml.h:3
#, no-c-format
msgid "Over (%)"
msgstr "Över (%)"

#: ../src/ui/sessions.xml.h:1
msgid "Open session"
msgstr "Öppna session"

#: ../src/ui/sessions_edit.xml.h:1
msgid "Session name:"
msgstr "Sessionsnamn:"

#: ../src/ui/sessions_edit.xml.h:2
msgid "Password:"
msgstr "Lösenord:"

#: ../src/ui/sessions_edit.xml.h:3
msgid "Username:"
msgstr "Användarnamn:"

#: ../src/ui/sessions_edit.xml.h:6
msgid "Session details"
msgstr "Sessionsdetaljer"

#: ../src/ui/sessions_edit.xml.h:7 ../tests/gyrus-talk.xml.h:3
msgid "Use a secure connection"
msgstr "Använd en säker anslutning"

#: ../src/ui/sessions_edit.xml.h:8
msgid "Mailbox hierarchy separator:"
msgstr "Avgränsare för postlådehierarki:"

#: ../src/ui/sessions_edit.xml.h:9
msgid "<b>Options</b>"
msgstr "<b>Alternativ</b>"

#: ../src/gyrus-admin-acl.c:54 ../src/gyrus-admin-acl.c:103
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr "Postlådan ”%s” finns inte."

#: ../src/gyrus-admin-acl.c:76
msgid "Invalid identifier."
msgstr "Ogiltig identifierare."

#: ../src/gyrus-admin-acl.c:82
msgid "Empty entry name."
msgstr "Blankt postnamn."

#: ../src/gyrus-admin-acl.c:87
msgid "Empty mailbox name."
msgstr "Blankt postlådenamn."

#: ../src/gyrus-admin-acl.c:107
msgid "Missing required argument to Setacl"
msgstr "Saknar nödvändigt argument till Setacl"

#: ../src/gyrus-admin-acl.c:143 ../src/gyrus-admin-mailbox.c:80
msgid "Permission denied"
msgstr "Åtkomst nekas"

#: ../src/gyrus-admin-acl.c:175
msgid "Empty access control list."
msgstr "Tom åtkomstkontrollista."

#: ../src/gyrus-admin-mailbox.c:78
msgid "Quota does not exist"
msgstr "Kvoten finns inte"

#: ../src/gyrus-admin-mailbox.c:172
#, c-format
msgid "Quota overloaded"
msgstr "Kvoten överskriden"

#: ../src/gyrus-admin-mailbox.c:230
msgid "Quota not valid. Please try again."
msgstr "Kvoten är inte giltig. Försök igen."

#: ../src/gyrus-admin-mailbox.c:244
msgid ""
"Unable to change quota. Are you sure do you have the appropriate permissions?"
msgstr ""
"Kunde inte ändra kvoten. Är du säker på att du har de lämpliga rättigheterna?"

#: ../src/gyrus-admin-mailbox.c:324
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr "”%s” är inte ett giltigt postlådenamn. Försök med ett annat namn."

#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr ""
"Föräldrapostlådan ”%s” finns inte. Uppdatera postlådelistan och försök igen."

#: ../src/gyrus-admin-mailbox.c:342
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr "Postlådan ”%s” finns redan. Försök med ett annat namn."

#: ../src/gyrus-admin-mailbox.c:355
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropriate "
"permissions?"
msgstr ""
"Kunde inte skapa postlådan. Är du säker på att du har de lämpliga "
"rättigheterna?"

#: ../src/gyrus-admin-mailbox.c:366
msgid "Mailbox created, but could not set quota."
msgstr "Postlåda skapad men kunde inte ställa in kvot."

#: ../src/gyrus-admin-mailbox.c:433
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr "Kunde inte ta bort ”%s”. Åtkomst nekad."

#: ../src/gyrus-admin-mailbox.c:634
msgid "new entry"
msgstr "ny post"

#: ../src/gyrus-admin.c:447 ../src/gyrus-report.c:287
#, c-format
msgid "Users (%d)"
msgstr "Användare (%d)"

#: ../src/gyrus-admin.c:453
#, c-format
msgid "Orphaned mailboxes (%d)"
msgstr "Föräldralösa postlådor (%d)"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:852
msgid "Orphaned mailboxes"
msgstr "Föräldralösa postlådor"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:850 ../src/gyrus-report.c:168
msgid "Users"
msgstr "Användare"

#: ../src/gyrus-admin.c:565
msgid "lookup"
msgstr "slå upp"

#: ../src/gyrus-admin.c:566
msgid "read"
msgstr "läs"

#: ../src/gyrus-admin.c:567
msgid "seen"
msgstr "läst"

#: ../src/gyrus-admin.c:568
msgid "write"
msgstr "skriv"

#: ../src/gyrus-admin.c:569
msgid "insert"
msgstr "infoga"

#: ../src/gyrus-admin.c:570
msgid "post"
msgstr "post"

#: ../src/gyrus-admin.c:571
msgid "create"
msgstr "skapa"

#: ../src/gyrus-admin.c:572
msgid "delete"
msgstr "ta bort"

#: ../src/gyrus-admin.c:573
msgid "admin"
msgstr "administrera"

#: ../src/gyrus-admin.c:581
msgid "Identifier"
msgstr "Identifierare"

#: ../src/gyrus-admin.c:651
msgid "Couldn't create the client socket."
msgstr "Kunde inte skapa klientuttaget."

#: ../src/gyrus-admin.c:659
msgid "Couldn't parse the server address."
msgstr "Kunde inte tolka serveradressen."

#: ../src/gyrus-admin.c:668
#, c-format
msgid "Could not connect to %s, port %d."
msgstr "Kunde inte ansluta till %s, port %d."

#: ../src/gyrus-admin.c:998
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr "Kunde inte anslut med blanka lösenord. Ange ditt lösenord."

#: ../src/gyrus-admin.c:1005
msgid "Incorrect login/password"
msgstr "Felaktigt inloggningsnamn/lösenord"

#: ../src/gyrus-admin.c:1361
msgid "Could not change permission. Server error: "
msgstr "Kunde inte ändra rättighet. Serverfel: "

#: ../src/gyrus-admin.c:1404
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr "En post kallad ”%s” finns redan. Skriva över den?"

#: ../src/gyrus-dialog-find-mailbox.c:171
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr "Texten ”%s” hittades inte i postlådelistan."

#: ../src/gyrus-dialog-find-mailbox.c:275
msgid "Find mailbox"
msgstr "Sök postlåda"

#: ../src/gyrus-dialog-mailbox-new.c:93
msgid "Quota not valid"
msgstr "Kvoten är inte giltig"

#: ../src/gyrus-dialog-mailbox-new.c:220
msgid "New mailbox"
msgstr "Ny postlåda"

#: ../src/gyrus-main-app.c:146
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr "Vill du verkligen ta bort postlådan ”%s” och alla dess underpostlådor?"

#: ../src/gyrus-main-app.c:261 ../src/gyrus-main-app.c:407
#: ../src/gyrus-main-app.c:703
msgid "Cyrus IMAP Administrator"
msgstr "Cyrus IMAP-administratör"

#: ../src/gyrus-main-app.c:275
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr "%s - Cyrus IMAP-administratör"

#: ../src/gyrus-main-app.c:354
msgid "_File"
msgstr "_Arkiv"

#: ../src/gyrus-main-app.c:355
msgid "_Edit"
msgstr "_Redigera"

#: ../src/gyrus-main-app.c:356
msgid "_ACL"
msgstr "_ACL"

#: ../src/gyrus-main-app.c:357
msgid "_View"
msgstr "_Visa"

#: ../src/gyrus-main-app.c:358
msgid "_Help"
msgstr "_Hjälp"

#: ../src/gyrus-main-app.c:359
msgid "Go to server..."
msgstr "Gå till server…"

#: ../src/gyrus-main-app.c:360
msgid "Show the list of servers"
msgstr "Visa listan på servrar"

#: ../src/gyrus-main-app.c:368
msgid "Add mailbox"
msgstr "Lägg till postlåda"

#: ../src/gyrus-main-app.c:369
msgid "Add a mailbox under the one selected"
msgstr "Lägg till en postlåda under den markerade"

#: ../src/gyrus-main-app.c:371
msgid "Search for a mailbox in current server"
msgstr "Sök efter en postlåda på aktuell server"

#: ../src/gyrus-main-app.c:373
msgid "Refresh the mailbox list"
msgstr "Uppdatera postlådelistan"

#: ../src/gyrus-main-app.c:374
msgid "Create report..."
msgstr "Skapa rapport…"

#: ../src/gyrus-main-app.c:375
msgid "Create report of users with quota problems"
msgstr "Skapa rapport på användare med kvotproblem"

#: ../src/gyrus-main-app.c:379
msgid "New entry"
msgstr "Ny post"

#: ../src/gyrus-main-app.c:380
msgid "Create a new ACL entry in current mailbox"
msgstr "Skapa en ny ACL-post i aktuell postlåda"

#: ../src/gyrus-main-app.c:381
msgid "Remove mailbox"
msgstr "Ta bort postlåda"

#: ../src/gyrus-main-app.c:382
msgid "Remove current mailbox from the server"
msgstr "Ta bort aktuell postlåda från servern"

#: ../src/gyrus-main-app.c:387
msgid "Rename entry"
msgstr "Byt namn på post"

#: ../src/gyrus-main-app.c:388
msgid "Rename selected ACL entry"
msgstr "Byt namn på markerad ACL-post"

#: ../src/gyrus-main-app.c:389
msgid "Delete entry"
msgstr "Ta bort post"

#: ../src/gyrus-main-app.c:390
msgid "Delete selected ACL entry"
msgstr "Ta bort markerad ACL-post"

#: ../src/gyrus-main-app.c:523
msgid "translators-credits"
msgstr ""
"Daniel Nylander <po@danielnylander.se>\n"
"Christian Rose\n"
"Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"\n"
"Skicka synpunkter på översättningen till\n"
"<tp-sv@listor.tp-sv.se>."

#: ../src/gyrus-main-app.c:533
msgid "GNOME Cyrus Administrator"
msgstr "GNOME Cyrus-administratör"

#: ../src/gyrus-main-app.c:535
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""
"© 2003-2005 GNOME Foundation\n"
"© 2004-2005 Claudio Saavedra"

#: ../src/gyrus-main-app.c:537
msgid "Administration tool for Cyrus IMAP servers."
msgstr "Administrationsverktyg för Cyrus IMAP-servrar."

#. set title
#: ../src/gyrus-report.c:105
#, c-format
msgid "Mailbox space usage report for %s"
msgstr "Användningsrapport för postlådeutrymme för %s"

#: ../src/gyrus-report.c:189 ../src/gyrus-report.c:490
msgid "Quota (%)"
msgstr "Kvot (%)"

#: ../src/gyrus-report.c:202 ../src/gyrus-report.c:494
msgid "Assigned (KB)"
msgstr "Tilldelat (KB)"

#: ../src/gyrus-report.c:213 ../src/gyrus-report.c:498
msgid "Used (KB)"
msgstr "Använt (KB)"

#. Translators: this represents the number of pages being printed.
#: ../src/gyrus-report.c:463
#, c-format
msgid "%d/%d"
msgstr "%d/%d"

#: ../src/gyrus-report.c:486
msgid "User"
msgstr "Användare"

#: ../src/gyrus-session.c:164
msgid "Edit session"
msgstr "Redigera session"

#: ../src/gyrus-session.c:178
msgid "New session"
msgstr "Ny session"

#: ../src/gyrus-session.c:357
msgid "A session name is required."
msgstr "Ett sessionsnamn krävs."

#: ../src/gyrus-session.c:368
#, c-format
msgid "Session named \"%s\" already exists."
msgstr "En session med namnet ”%s” finns redan."

#: ../src/gyrus-session.c:464 ../src/gyrus-session.c:598
msgid "Autodetect"
msgstr "Identifiera automatiskt"

#: ../src/gyrus-session.c:549
msgid "Session"
msgstr "Session"

#: ../src/gyrus-session.c:718
msgid "No host specified."
msgstr "Ingen värd angiven."

#: ../tests/gyrus-talk.xml.h:1
msgid "Talk - Echo client"
msgstr "Talk - Ekoklient"

#: ../tests/gyrus-talk.xml.h:5
msgid "_Connect"
msgstr "_Anslut"

#: ../tests/gyrus-talk.xml.h:6
msgid "Connection"
msgstr "Anslutning"

#: ../tests/gyrus-talk.xml.h:7
msgid "Command:"
msgstr "Kommando:"

#: ../tests/gyrus-talk.xml.h:8
msgid "_Send"
msgstr "_Skicka"

#~ msgid "<b>Name:</b>"
#~ msgstr "<b>Namn:</b>"

#~ msgid "<b>Quota (MB):</b>"
#~ msgstr "<b>Kvot (MB):</b>"

#~ msgid "<b>Free space:</b>"
#~ msgstr "<b>Ledigt utrymme:</b>"

#~ msgid "<b>Host:</b>"
#~ msgstr "<b>Värd:</b>"

#~ msgid "<b>Owner:</b>"
#~ msgstr "<b>Ägare:</b>"

#~ msgid "<b>Port:</b>"
#~ msgstr "<b>Port:</b>"

#~ msgid "<b>User:</b>"
#~ msgstr "<b>Användare:</b>"

#~ msgid "<b>Connection</b>"
#~ msgstr "<b>Anslutning</b>"

#~ msgid "*"
#~ msgstr "*"

#~ msgid "Create mailbox"
#~ msgstr "Skapa postlåda"

#~ msgid "Find"
#~ msgstr "Sök"

#~ msgid "<b>Mailbox quota:</b>"
#~ msgstr "<b>Postlådekvot:</b>"

#~ msgid "<b>Mailboxes tree:</b>"
#~ msgstr "<b>Påstlådeträd:</b>"

#~ msgid "Default suffix for changing quota:"
#~ msgstr "Standardändelse för ändring av kvot:"

#~ msgid "Preferences"
#~ msgstr "Inställningar"

#~ msgid "View complete mailboxes tree"
#~ msgstr "Visa komplett träd över postlådor"

#~ msgid ""
#~ ".\n"
#~ "/\n"
#~ "Autodetect"
#~ msgstr ""
#~ ".\n"
#~ "/\n"
#~ "Identifiera automatiskt"

#~ msgid "%s could not be found. Please check the name and try again."
#~ msgstr "%s kunde inte hittas. Kontrollera namnet och försök igen."

#~ msgid "_Disconnect"
#~ msgstr "_Koppla ned"

#~ msgid "Report: %s"
#~ msgstr "Rapport: %s"

#~ msgid "Print Report"
#~ msgstr "Skriv ut rapport"

#~ msgid "Preview"
#~ msgstr "Förhandsgranska"

#~ msgid "<small><i><b>Note: </b>Uses a hostname or IP address</i></small>"
#~ msgstr ""
#~ "<small><i><b>Notera: </b>Använder ett värdnamn eller IP-adress</i></small>"

#~ msgid "Save as:"
#~ msgstr "Spara som:"

#~ msgid "_Quit"
#~ msgstr "_Avsluta"

#~ msgid "Quit the program"
#~ msgstr "Avsluta programmet"

#~ msgid "_About"
#~ msgstr "_Om"

#~ msgid "_Find"
#~ msgstr "_Sök"

#~ msgid "_Refresh"
#~ msgstr "_Uppdatera"

#~ msgid "Host (or IP address)"
#~ msgstr "Värd (eller IP-adress)"

#~ msgid "(c) 2003 GNOME Foundation"
#~ msgstr "© 2003 GNOME Foundation"

#~ msgid ""
#~ "B\n"
#~ "KB\n"
#~ "MB\n"
#~ "GB\n"
#~ "TB"
#~ msgstr ""
#~ "B\n"
#~ "kB\n"
#~ "MB\n"
#~ "GB\n"
#~ "TB"

#~ msgid "Could not change quota."
#~ msgstr "Kunde inte ändra kvot."

#, fuzzy
#~ msgid "Mailbox '%s' does not exists."
#~ msgstr "Filen %s finns inte"

#~ msgid "© 2003 GNOME Foundation"
#~ msgstr "© 2003 GNOME Foundation"
