# Finnish messages for gyrus.
# Copyright (C) 2006 Ilkka Tuohela. 
# Suomennos: http://gnome-fi.sourceforge.net/
# Ilkka Tuohela <hile@iki.fi>, 2006.
# Jiri Grönroos <jiri.gronroos+l10n@iki.fi>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gyrus\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gyrus&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-12-19 14:26+0000\n"
"PO-Revision-Date: 2013-09-15 14:50+0300\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>\n"
"Language-Team: suomi <gnome-fi-laatu@lists.sourceforge.net>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.6\n"

#: ../gyrus.desktop.in.in.h:1
msgid "Gyrus IMAP Cyrus Administrator"
msgstr "Gyrus - Cyrus-/IMAP-palvelimen hallinta"

#: ../gyrus.desktop.in.in.h:2
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr "Hallitse Cyrus-/IMAP-palvelimen postilaatikoita"

#: ../src/ui/create_mailbox.xml.h:1
msgid "Name:"
msgstr "Nimi:"

#: ../src/ui/create_mailbox.xml.h:2
msgid "Quota (MB):"
msgstr "Levytila (Mt):"

#: ../src/ui/create_mailbox.xml.h:3
msgid "Assign quota"
msgstr "Määrittele levytila"

#: ../src/ui/find.xml.h:1
msgid "Search for:"
msgstr "Etsittävä merkkijono:"

#: ../src/ui/find.xml.h:2
msgid "Match _entire word only"
msgstr "Täsmää _vain koko sanoihin"

#: ../src/ui/find.xml.h:3
msgid "_Wrap around"
msgstr "_Rivitä"

#: ../src/ui/page.xml.h:1
msgid "Free space:"
msgstr "Vapaa levytila:"

#: ../src/ui/page.xml.h:2
msgid "Assigned space:"
msgstr "Myönnetty levytila:"

#: ../src/ui/page.xml.h:3
msgid "Owner:"
msgstr "Omistaja:"

#: ../src/ui/page.xml.h:4
msgid "New quota (MB)"
msgstr "Uusi levytila (Mt)"

#: ../src/ui/page.xml.h:5
msgid "Modify quota"
msgstr "Muuta levytilaa"

#: ../src/ui/page.xml.h:6
msgid "Access control list"
msgstr "Pääsylista"

#: ../src/ui/page.xml.h:7 ../src/ui/sessions_edit.xml.h:5
#: ../tests/gyrus-talk.xml.h:2
msgid "Host:"
msgstr "Palvelin:"

#: ../src/ui/page.xml.h:8
msgid "User:"
msgstr "Käyttäjä:"

#: ../src/ui/page.xml.h:9 ../src/ui/sessions_edit.xml.h:4
#: ../tests/gyrus-talk.xml.h:4
msgid "Port:"
msgstr "Portti:"

#: ../src/ui/password.xml.h:1
msgid "Password"
msgstr "Salasana"

#: ../src/ui/password.xml.h:2
msgid "Enter your password"
msgstr "Syötä salasana"

#: ../src/ui/report.xml.h:1
msgid "Report"
msgstr "Raportti"

#: ../src/ui/report.xml.h:3
#, no-c-format
msgid "Over (%)"
msgstr "Yli (%)"

#: ../src/ui/sessions.xml.h:1
msgid "Open session"
msgstr "Avaa istunto"

#: ../src/ui/sessions_edit.xml.h:1
msgid "Session name:"
msgstr "Istunto nimi:"

#: ../src/ui/sessions_edit.xml.h:2
msgid "Password:"
msgstr "Salasana:"

#: ../src/ui/sessions_edit.xml.h:3
msgid "Username:"
msgstr "Käyttäjätunnus:"

#: ../src/ui/sessions_edit.xml.h:6
msgid "Session details"
msgstr "Istunnon yksityiskohdat"

#: ../src/ui/sessions_edit.xml.h:7 ../tests/gyrus-talk.xml.h:3
msgid "Use a secure connection"
msgstr "Käytä salattua yhteyttä"

#: ../src/ui/sessions_edit.xml.h:8
msgid "Mailbox hierarchy separator:"
msgstr "Postilaatikon hierarkian erotin:"

#: ../src/ui/sessions_edit.xml.h:9
msgid "<b>Options</b>"
msgstr "<b>Valinnat</b>"

#: ../src/gyrus-admin-acl.c:54 ../src/gyrus-admin-acl.c:103
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr "Sähköpostikansiota \"%s\" ei ole olemassa."

#: ../src/gyrus-admin-acl.c:76
msgid "Invalid identifier."
msgstr "Virheellinen tunniste"

#: ../src/gyrus-admin-acl.c:82
msgid "Empty entry name."
msgstr "Syötetty nimi on tyhjä."

#: ../src/gyrus-admin-acl.c:87
msgid "Empty mailbox name."
msgstr "Tyhjä postilaatikon nimi."

#: ../src/gyrus-admin-acl.c:107
msgid "Missing required argument to Setacl"
msgstr "Komennon Setacl vaadittu argumentti puuttuu"

#: ../src/gyrus-admin-acl.c:143 ../src/gyrus-admin-mailbox.c:80
msgid "Permission denied"
msgstr "Lupa evätty"

#: ../src/gyrus-admin-acl.c:175
msgid "Empty access control list."
msgstr "Tyhjä pääsylista."

#: ../src/gyrus-admin-mailbox.c:78
msgid "Quota does not exist"
msgstr "Levytilaa ei ole määritelty"

#: ../src/gyrus-admin-mailbox.c:172
#, c-format
msgid "Quota overloaded"
msgstr "Levytila on ylitetty"

#: ../src/gyrus-admin-mailbox.c:230
msgid "Quota not valid. Please try again."
msgstr "Levytilan määrittely ei kelpaa, yritä uudestaan."

#: ../src/gyrus-admin-mailbox.c:244
msgid ""
"Unable to change quota. Are you sure do you have the appropriate permissions?"
msgstr "Levytilan muuttaminen ei onnistunut. Riittävätkö oikeutesi toimintoon?"

#: ../src/gyrus-admin-mailbox.c:324
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr "\"%s\" ei ole kelvollinen kansion nimi, käytä toista nimeä."

#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr ""
"Ylemmän tason kansi \"%s\" ei ole olemassa. Päivitä kansiolista ja yritä "
"uudestaan."

#: ../src/gyrus-admin-mailbox.c:342
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr "Postilaatikko \"%s\" on jo olemassa, valitse toinen nimi."

#: ../src/gyrus-admin-mailbox.c:355
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropriate "
"permissions?"
msgstr "Kansiota ei voi luoda. Riittävätkö oikeutesi varmasti tähän?"

#: ../src/gyrus-admin-mailbox.c:366
msgid "Mailbox created, but could not set quota."
msgstr "Postilaatikko on luotu, mutta levytilaa ei voitu asettaa."

#: ../src/gyrus-admin-mailbox.c:433
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr "Kansiota \"%s\" ei voi poistaa, lupa evätty."

#: ../src/gyrus-admin-mailbox.c:634
msgid "new entry"
msgstr "uusi kohta"

#: ../src/gyrus-admin.c:447 ../src/gyrus-report.c:287
#, c-format
msgid "Users (%d)"
msgstr "Käyttäjät (%d)"

#: ../src/gyrus-admin.c:453
#, c-format
msgid "Orphaned mailboxes (%d)"
msgstr "Hylätyt sähköpostikansiot (%d)"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:852
msgid "Orphaned mailboxes"
msgstr "Hylätyt sähköpostikansiot"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:850 ../src/gyrus-report.c:168
msgid "Users"
msgstr "Käyttäjät"

#: ../src/gyrus-admin.c:565
msgid "lookup"
msgstr "etsi"

#: ../src/gyrus-admin.c:566
msgid "read"
msgstr "luku"

#: ../src/gyrus-admin.c:567
msgid "seen"
msgstr "nähty"

#: ../src/gyrus-admin.c:568
msgid "write"
msgstr "kirjoitus"

#: ../src/gyrus-admin.c:569
msgid "insert"
msgstr "lisäys"

#: ../src/gyrus-admin.c:570
msgid "post"
msgstr "postitus"

#: ../src/gyrus-admin.c:571
msgid "create"
msgstr "luonti"

#: ../src/gyrus-admin.c:572
msgid "delete"
msgstr "poisto"

#: ../src/gyrus-admin.c:573
msgid "admin"
msgstr "ylläpito"

#: ../src/gyrus-admin.c:581
msgid "Identifier"
msgstr "Tunniste"

#: ../src/gyrus-admin.c:651
msgid "Couldn't create the client socket."
msgstr ""

#: ../src/gyrus-admin.c:659
msgid "Couldn't parse the server address."
msgstr ""

#: ../src/gyrus-admin.c:668
#, c-format
msgid "Could not connect to %s, port %d."
msgstr "Palvelimeen \"%s\" ei saatu yhteyttä (portti %d)."

#: ../src/gyrus-admin.c:998
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr "Ei voitu yhdistää tyhjällä salasanalla, anna salasanalla."

#: ../src/gyrus-admin.c:1005
msgid "Incorrect login/password"
msgstr "Väärä käyttäjätunnus tai salasana"

#: ../src/gyrus-admin.c:1361
msgid "Could not change permission. Server error: "
msgstr "Oikeuksia ei voi muuttaa, palvelinvirhe: "

#: ../src/gyrus-admin.c:1404
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr "Kohta nimeltä \"%s\" on jo olemassa, kirjoitetaanko sen yli?"

#: ../src/gyrus-dialog-find-mailbox.c:171
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr "Merkkijonoa \"%s\" ei löydy postikansioiden listasta."

#: ../src/gyrus-dialog-find-mailbox.c:275
msgid "Find mailbox"
msgstr "Etsi postilaatikko"

#: ../src/gyrus-dialog-mailbox-new.c:93
msgid "Quota not valid"
msgstr "Virheellinen levytilan määrittely"

#: ../src/gyrus-dialog-mailbox-new.c:220
msgid "New mailbox"
msgstr "Uusi postilaatikko"

#: ../src/gyrus-main-app.c:146
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr "Poistetaanko todella postikansio \"%s\" ja kaikki sen alikansiot?"

#: ../src/gyrus-main-app.c:261 ../src/gyrus-main-app.c:407
#: ../src/gyrus-main-app.c:703
msgid "Cyrus IMAP Administrator"
msgstr "Cyrus-/IMAP-palvelimen hallinta"

#: ../src/gyrus-main-app.c:275
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr "%s - Cyrus-/IMAP-palvelimen hallinta"

#: ../src/gyrus-main-app.c:354
msgid "_File"
msgstr "_Tiedosto"

#: ../src/gyrus-main-app.c:355
msgid "_Edit"
msgstr "_Muokkaa"

#: ../src/gyrus-main-app.c:356
msgid "_ACL"
msgstr "_ACL"

#: ../src/gyrus-main-app.c:357
msgid "_View"
msgstr "_Näytä"

#: ../src/gyrus-main-app.c:358
msgid "_Help"
msgstr "O_hje"

#: ../src/gyrus-main-app.c:359
msgid "Go to server..."
msgstr "Siirry palvelimelle..."

#: ../src/gyrus-main-app.c:360
msgid "Show the list of servers"
msgstr "Näytä lista palvelimista"

#: ../src/gyrus-main-app.c:368
msgid "Add mailbox"
msgstr "Lisää postilaatikko"

#: ../src/gyrus-main-app.c:369
msgid "Add a mailbox under the one selected"
msgstr "Lisää postilaatikko valitun alakansioksi"

#: ../src/gyrus-main-app.c:371
msgid "Search for a mailbox in current server"
msgstr "Etsi postilaatikkoa valitulta palvelimelta"

#: ../src/gyrus-main-app.c:373
msgid "Refresh the mailbox list"
msgstr "Päivitä lista postilaatikoista"

#: ../src/gyrus-main-app.c:374
msgid "Create report..."
msgstr "Luo raportti..."

#: ../src/gyrus-main-app.c:375
msgid "Create report of users with quota problems"
msgstr "Luo raportti käyttäjistä, joilla on levytilaongelmia"

#: ../src/gyrus-main-app.c:379
msgid "New entry"
msgstr "Uusi kohta"

#: ../src/gyrus-main-app.c:380
msgid "Create a new ACL entry in current mailbox"
msgstr "Luo uusi pääsylistan määrittely valitulle postilaatikolle"

#: ../src/gyrus-main-app.c:381
msgid "Remove mailbox"
msgstr "Poista postilaatikko"

#: ../src/gyrus-main-app.c:382
msgid "Remove current mailbox from the server"
msgstr "Poista tämä sähköpostikansio palvelimelta"

#: ../src/gyrus-main-app.c:387
msgid "Rename entry"
msgstr "Nimeä kohta uudestaan"

#: ../src/gyrus-main-app.c:388
msgid "Rename selected ACL entry"
msgstr "Nimeä valittu ACL-kohta uudestaan"

#: ../src/gyrus-main-app.c:389
msgid "Delete entry"
msgstr "Poista kohta"

#: ../src/gyrus-main-app.c:390
msgid "Delete selected ACL entry"
msgstr "Poista valittu ACL-kohta"

#: ../src/gyrus-main-app.c:523
msgid "translators-credits"
msgstr ""
"Ilkka Tuohela, 2006\n"
"\n"
"https://l10n.gnome.org/teams/fi/"

#: ../src/gyrus-main-app.c:533
msgid "GNOME Cyrus Administrator"
msgstr "Gnomen Cyrus-palvelimen hallinta"

#: ../src/gyrus-main-app.c:535
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"

#: ../src/gyrus-main-app.c:537
msgid "Administration tool for Cyrus IMAP servers."
msgstr "Hallintatyökalu Cyrus-/IMAP-palvelimille."

#. set title
#: ../src/gyrus-report.c:105
#, c-format
msgid "Mailbox space usage report for %s"
msgstr "Postilaatikon tilankäyttöraportti käyttäjälle %s"

#: ../src/gyrus-report.c:189 ../src/gyrus-report.c:490
msgid "Quota (%)"
msgstr "Levytila (%)"

#: ../src/gyrus-report.c:202 ../src/gyrus-report.c:494
msgid "Assigned (KB)"
msgstr "Myönnetty (kt)"

#: ../src/gyrus-report.c:213 ../src/gyrus-report.c:498
msgid "Used (KB)"
msgstr "Käytössä (kt)"

#. Translators: this represents the number of pages being printed.
#: ../src/gyrus-report.c:463
#, c-format
msgid "%d/%d"
msgstr "%d/%d"

#: ../src/gyrus-report.c:486
msgid "User"
msgstr "Käyttäjä"

#: ../src/gyrus-session.c:164
msgid "Edit session"
msgstr "Muokkaa istuntoa"

#: ../src/gyrus-session.c:178
msgid "New session"
msgstr "Uusi istunto"

#: ../src/gyrus-session.c:357
msgid "A session name is required."
msgstr "Istunnon nimi vaaditaan."

#: ../src/gyrus-session.c:368
#, c-format
msgid "Session named \"%s\" already exists."
msgstr "Istunto nimeltä \"%s\" on jo olemassa."

#: ../src/gyrus-session.c:464 ../src/gyrus-session.c:598
msgid "Autodetect"
msgstr "Tunnista automaattisesti"

#: ../src/gyrus-session.c:549
msgid "Session"
msgstr "Istunto"

#: ../src/gyrus-session.c:718
msgid "No host specified."
msgstr "Verkkonimeä ei annettu."

#: ../tests/gyrus-talk.xml.h:1
msgid "Talk - Echo client"
msgstr "Talk - Echo-asiakas"

#: ../tests/gyrus-talk.xml.h:5
msgid "_Connect"
msgstr "_Avaa yhteys"

#: ../tests/gyrus-talk.xml.h:6
msgid "Connection"
msgstr "Yhteys"

#: ../tests/gyrus-talk.xml.h:7
msgid "Command:"
msgstr "Komento:"

#: ../tests/gyrus-talk.xml.h:8
msgid "_Send"
msgstr "_Lähetä"

#~ msgid "*"
#~ msgstr "*"

#~ msgid "<b>Name:</b>"
#~ msgstr "<b>Nimi:</b>"

#~ msgid "<b>Quota (MB):</b>"
#~ msgstr "<b>Levytila (Mt):</b>"

#~ msgid "Create mailbox"
#~ msgstr "Luo postilaatikko"

#~ msgid "Find"
#~ msgstr "Etsi"

#~ msgid "<b>Host:</b>"
#~ msgstr "<b>Palvelin:</b>"

#~ msgid "<b>Port:</b>"
#~ msgstr "<b>Portti:</b>"

#~ msgid "<b>User:</b>"
#~ msgstr "<b>Käyttäjä:</b>"

#~ msgid "<b>Mailbox quota:</b>"
#~ msgstr "<b>Sähköpostin levytila:</b>"

#~ msgid "<b>Mailboxes tree:</b>"
#~ msgstr "<b>Sähköpostikansiopuu:</b>"

#~ msgid "Default suffix for changing quota:"
#~ msgstr "Levytilaa muutettaessa käytetty oletuspääte:"

#~ msgid "Preferences"
#~ msgstr "Asetukset"

#~ msgid "View complete mailboxes tree"
#~ msgstr "Näytä koko postikansioiden puu"

#~ msgid ""
#~ ".\n"
#~ "/\n"
#~ "Autodetect"
#~ msgstr ""
#~ ".\n"
#~ "/\n"
#~ "Tunnista automaattisesti"

#~ msgid "%s could not be found. Please check the name and try again."
#~ msgstr "Kansiota %s ei löydy, tarkista nimi ja yritä uudestaan."

#~ msgid "_Disconnect"
#~ msgstr "_Katkaise yhteys"

#~ msgid "Report: %s"
#~ msgstr "Raportti: %s"

#~ msgid "Print Report"
#~ msgstr "Tulosta raportti"

#~ msgid "Preview"
#~ msgstr "Esikatselu"

#~ msgid "<b>Connection</b>"
#~ msgstr "<b>Yhteys</b>"

#~ msgid "Save as:"
#~ msgstr "Tallenna nimellä:"

#~ msgid "_Quit"
#~ msgstr "_Sulje"

#~ msgid "Quit the program"
#~ msgstr "Sulje ohjelma"

#~ msgid "_About"
#~ msgstr "_Tietoja"

#~ msgid "_Find"
#~ msgstr "_Etsi"

#~ msgid "_Refresh"
#~ msgstr "_Päivitä"
