# Danish translation for gyrus.
# Copyright (C) 2013 gyrus & nedenstående oversætttere.
# This file is distributed under the same license as the gyrus package.
# Joe Hansen <joedalton2@yahoo.dk>, 2010, 2013,
#
# appropriate -> nødvendige
# entry -> punkt
# exists -> findes
#
msgid ""
msgstr ""
"Project-Id-Version: gyrus master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-03-24 16:09+0100\n"
"PO-Revision-Date: 2013-03-18 19:00+0000\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../gyrus.desktop.in.in.h:1
msgid "Gyrus IMAP Cyrus Administrator"
msgstr "Gyrus - administrator til IMAP Cyrus"

#: ../gyrus.desktop.in.in.h:2
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr "Administrer postkasserne på dine IMAP Cyrus-servere"

#: ../src/ui/create_mailbox.xml.h:1
msgid "Name:"
msgstr "Navn:"

#: ../src/ui/create_mailbox.xml.h:2
msgid "Quota (MB):"
msgstr "Kvote (MB):"

#: ../src/ui/create_mailbox.xml.h:3
msgid "Assign quota"
msgstr "Tildel kvote"

#: ../src/ui/find.xml.h:1
msgid "Search for:"
msgstr "Søg efter:"

#: ../src/ui/find.xml.h:2
msgid "Match _entire word only"
msgstr "Find kun _hele ord"

#: ../src/ui/find.xml.h:3
msgid "_Wrap around"
msgstr "_Ombryd linjer"

#: ../src/ui/page.xml.h:1
msgid "Free space:"
msgstr "Ledig plads:"

#: ../src/ui/page.xml.h:2
msgid "Assigned space:"
msgstr "Tildelt plads:"

#: ../src/ui/page.xml.h:3
msgid "Owner:"
msgstr "Ejer:"

#: ../src/ui/page.xml.h:4
msgid "New quota (MB)"
msgstr "Ny kvote (MB)"

#: ../src/ui/page.xml.h:5
msgid "Modify quota"
msgstr "Ændr kvote"

#: ../src/ui/page.xml.h:6
msgid "Access control list"
msgstr "Tilgangskontrolliste"

#: ../src/ui/page.xml.h:7 ../src/ui/sessions_edit.xml.h:5
#: ../tests/gyrus-talk.xml.h:2
msgid "Host:"
msgstr "Vært:"

#: ../src/ui/page.xml.h:8
msgid "User:"
msgstr "Bruger:"

#: ../src/ui/page.xml.h:9 ../src/ui/sessions_edit.xml.h:4
#: ../tests/gyrus-talk.xml.h:4
msgid "Port:"
msgstr "Port:"

#: ../src/ui/password.xml.h:1
msgid "Password"
msgstr "Adgangskode"

#: ../src/ui/password.xml.h:2
msgid "Enter your password"
msgstr "Indtast din adgangskode"

#: ../src/ui/report.xml.h:1
msgid "Report"
msgstr "Rapport"

#: ../src/ui/report.xml.h:3
#, no-c-format
msgid "Over (%)"
msgstr "Over (%)"

#: ../src/ui/sessions.xml.h:1
msgid "Open session"
msgstr "Åbn session"

#: ../src/ui/sessions_edit.xml.h:1
msgid "Session name:"
msgstr "Sessionsnavn:"

#: ../src/ui/sessions_edit.xml.h:2
msgid "Password:"
msgstr "Adgangskode:"

#: ../src/ui/sessions_edit.xml.h:3
msgid "Username:"
msgstr "Brugernavn:"

#: ../src/ui/sessions_edit.xml.h:6
msgid "Session details"
msgstr "Sessionsdetaljer"

#: ../src/ui/sessions_edit.xml.h:7 ../tests/gyrus-talk.xml.h:3
msgid "Use a secure connection"
msgstr "Brug en sikker forbindelse"

#: ../src/ui/sessions_edit.xml.h:8
msgid "Mailbox hierarchy separator:"
msgstr "Adskillelsestegn for postkassehierarki:"

#: ../src/ui/sessions_edit.xml.h:9
msgid "<b>Options</b>"
msgstr "<b>Indstillinger</b>"

#: ../src/gyrus-admin-acl.c:54 ../src/gyrus-admin-acl.c:103
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr "Postkasse '%s' findes ikke."

#: ../src/gyrus-admin-acl.c:76
msgid "Invalid identifier."
msgstr "Ugyldig identifikator."

#: ../src/gyrus-admin-acl.c:82
msgid "Empty entry name."
msgstr "Tomt punktnavn."

#: ../src/gyrus-admin-acl.c:87
msgid "Empty mailbox name."
msgstr "Tomt postkassenavn."

#: ../src/gyrus-admin-acl.c:107
msgid "Missing required argument to Setacl"
msgstr "Påkrævet argument til Setacl mangler"

#: ../src/gyrus-admin-acl.c:143 ../src/gyrus-admin-mailbox.c:80
msgid "Permission denied"
msgstr "Tilladelse nægtet"

#: ../src/gyrus-admin-acl.c:175
msgid "Empty access control list."
msgstr "Tom adgangskontrolliste."

#: ../src/gyrus-admin-mailbox.c:78
msgid "Quota does not exist"
msgstr "Kvote findes ikke"

#: ../src/gyrus-admin-mailbox.c:172
#, c-format
msgid "Quota overloaded"
msgstr "Kvote overskredet"

#: ../src/gyrus-admin-mailbox.c:230
msgid "Quota not valid. Please try again."
msgstr "Kvote ugyldig. Forsøg venligst igen."

#: ../src/gyrus-admin-mailbox.c:244
msgid ""
"Unable to change quota. Are you sure do you have the appropriate permissions?"
msgstr ""
"Kunne ikke ændre kvote. Er du sikker på, at du har de nødvendige rettigheder?"

#: ../src/gyrus-admin-mailbox.c:324
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr ""
"'%s' er ikke et gyldigt postkassenavn. Forsøg venligst med et andet navn."

# (ja, tror det er postkasserne, der danner et træ, som tidligere nævnt)
#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr ""
"Overpostkasse '%s' findes ikke. Opdater venligst postkasselisten og forsøg "
"igen."

#: ../src/gyrus-admin-mailbox.c:342
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr "Postkasse '%s' findes allerede. Forsøg venligst et andet navn."

#: ../src/gyrus-admin-mailbox.c:355
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropriate "
"permissions?"
msgstr ""
"Kunne ikke oprette postkassen. Er du sikker på, at du har de nødvendige "
"tilladelser?"

#: ../src/gyrus-admin-mailbox.c:366
msgid "Mailbox created, but could not set quota."
msgstr "Postkasse oprettet, men kunne ikke angive kvote."

#: ../src/gyrus-admin-mailbox.c:433
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr "Kunne ikke slette '%s'. Tilladelse nægtet."

#: ../src/gyrus-admin-mailbox.c:634
msgid "new entry"
msgstr "nyt punkt"

#: ../src/gyrus-admin.c:447 ../src/gyrus-report.c:287
#, c-format
msgid "Users (%d)"
msgstr "Brugere (%d)"

#: ../src/gyrus-admin.c:453
#, c-format
msgid "Orphaned mailboxes (%d)"
msgstr "Forældreløse postkasser (%d)"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:852
msgid "Orphaned mailboxes"
msgstr "Forældreløse postkasser"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:850 ../src/gyrus-report.c:168
msgid "Users"
msgstr "Brugere"

#: ../src/gyrus-admin.c:565
msgid "lookup"
msgstr "opslag"

#: ../src/gyrus-admin.c:566
msgid "read"
msgstr "læst"

#: ../src/gyrus-admin.c:567
msgid "seen"
msgstr "set"

#: ../src/gyrus-admin.c:568
msgid "write"
msgstr "skriv"

#: ../src/gyrus-admin.c:569
msgid "insert"
msgstr "indsæt"

#: ../src/gyrus-admin.c:570
msgid "post"
msgstr "post"

#: ../src/gyrus-admin.c:571
msgid "create"
msgstr "opret"

#: ../src/gyrus-admin.c:572
msgid "delete"
msgstr "slet"

#: ../src/gyrus-admin.c:573
msgid "admin"
msgstr "admin"

#: ../src/gyrus-admin.c:581
msgid "Identifier"
msgstr "Identifikator"

#: ../src/gyrus-admin.c:651
msgid "Couldn't create the client socket."
msgstr "Kunne ikke oprette klientsoklen."

#: ../src/gyrus-admin.c:659
msgid "Couldn't parse the server address."
msgstr "Kunne ikke fortolke serveradressen."

#: ../src/gyrus-admin.c:668
#, c-format
msgid "Could not connect to %s, port %d."
msgstr "Kunne ikke forbinde til %s, port %d."

#: ../src/gyrus-admin.c:998
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr ""
"Kan ikke forbinde med tomme adgangskoder. Angiv venligst din adgangskode."

#: ../src/gyrus-admin.c:1005
msgid "Incorrect login/password"
msgstr "Forkert logind/adgangskode"

#: ../src/gyrus-admin.c:1361
msgid "Could not change permission. Server error: "
msgstr "Kunne ikke ændre tilladelse. Serverfejl: "

#: ../src/gyrus-admin.c:1404
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr "Et punkt kaldt '%s' findes allerede. Overskriv det?"

#: ../src/gyrus-dialog-find-mailbox.c:171
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr "Teksten '%s' blev ikke fundet i postkasselisten."

#: ../src/gyrus-dialog-find-mailbox.c:275
msgid "Find mailbox"
msgstr "Find postkasse"

#: ../src/gyrus-dialog-mailbox-new.c:93
msgid "Quota not valid"
msgstr "Kvote er ugyldig"

#: ../src/gyrus-dialog-mailbox-new.c:220
msgid "New mailbox"
msgstr "Ny postkasse"

#: ../src/gyrus-main-app.c:146
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr "Vil du virkelig slette postkassen '%s' og alle dens underpostkasser?"

#: ../src/gyrus-main-app.c:261 ../src/gyrus-main-app.c:407
#: ../src/gyrus-main-app.c:703
msgid "Cyrus IMAP Administrator"
msgstr "Cyrus IMAP-administrator"

#: ../src/gyrus-main-app.c:275
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr "%s - Cyrus IMAP-administrator"

#: ../src/gyrus-main-app.c:354
msgid "_File"
msgstr "_Fil"

#: ../src/gyrus-main-app.c:355
msgid "_Edit"
msgstr "_Rediger"

#: ../src/gyrus-main-app.c:356
msgid "_ACL"
msgstr "_ACL"

#: ../src/gyrus-main-app.c:357
msgid "_View"
msgstr "_Vis"

#: ../src/gyrus-main-app.c:358
msgid "_Help"
msgstr "_Hjælp"

#: ../src/gyrus-main-app.c:359
msgid "Go to server..."
msgstr "Gå til server..."

#: ../src/gyrus-main-app.c:360
msgid "Show the list of servers"
msgstr "Vis listen over servere"

#: ../src/gyrus-main-app.c:368
msgid "Add mailbox"
msgstr "Tilføj postkasse"

#: ../src/gyrus-main-app.c:369
msgid "Add a mailbox under the one selected"
msgstr "Tilføj en postkasse under den valgte"

#: ../src/gyrus-main-app.c:371
msgid "Search for a mailbox in current server"
msgstr "Søg efter en postkasse på den aktuelle server"

#: ../src/gyrus-main-app.c:373
msgid "Refresh the mailbox list"
msgstr "Genopdater postkasselisten"

#: ../src/gyrus-main-app.c:374
msgid "Create report..."
msgstr "Opret rapport..."

#: ../src/gyrus-main-app.c:375
msgid "Create report of users with quota problems"
msgstr "Opret rapport over brugere med kvoteproblemer"

#: ../src/gyrus-main-app.c:379
msgid "New entry"
msgstr "Nyt punkt"

#: ../src/gyrus-main-app.c:380
msgid "Create a new ACL entry in current mailbox"
msgstr "Opret et nyt ACL-punkt i aktuel postkasse"

#: ../src/gyrus-main-app.c:381
msgid "Remove mailbox"
msgstr "Fjern postkasse"

#: ../src/gyrus-main-app.c:382
msgid "Remove current mailbox from the server"
msgstr "Fjern aktuel postkasse fra serveren"

#: ../src/gyrus-main-app.c:387
msgid "Rename entry"
msgstr "Omdøb punkt"

#: ../src/gyrus-main-app.c:388
msgid "Rename selected ACL entry"
msgstr "Omdøb valgt ACL-punkt"

#: ../src/gyrus-main-app.c:389
msgid "Delete entry"
msgstr "Slet punkt"

#: ../src/gyrus-main-app.c:390
msgid "Delete selected ACL entry"
msgstr "Slet valgt ACL-punkt"

#: ../src/gyrus-main-app.c:523
msgid "translators-credits"
msgstr ""
"Joe Hansen, 2010, 2013.\n"
"\n"
"Dansk-gruppen <dansk@dansk-gruppen.dk>\n"
"Mere info: http://www.dansk-gruppen.dk"

#: ../src/gyrus-main-app.c:533
msgid "GNOME Cyrus Administrator"
msgstr "GNOME Cyrus-administrator"

#: ../src/gyrus-main-app.c:535
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"

#: ../src/gyrus-main-app.c:537
msgid "Administration tool for Cyrus IMAP servers."
msgstr "Administrationsværktøj til Cyrus IMAP-servere."

#. set title
#: ../src/gyrus-report.c:105
#, c-format
msgid "Mailbox space usage report for %s"
msgstr "Rapport over forbrug af postkasseplads for %s"

#: ../src/gyrus-report.c:189 ../src/gyrus-report.c:490
msgid "Quota (%)"
msgstr "Kvote (%)"

#: ../src/gyrus-report.c:202 ../src/gyrus-report.c:494
msgid "Assigned (KB)"
msgstr "Tildelt (KB)"

#: ../src/gyrus-report.c:213 ../src/gyrus-report.c:498
msgid "Used (KB)"
msgstr "Brugt (KB)"

#. Translators: this represents the number of pages being printed.
#: ../src/gyrus-report.c:463
#, c-format
msgid "%d/%d"
msgstr "%d/%d"

#: ../src/gyrus-report.c:486
msgid "User"
msgstr "Bruger"

#: ../src/gyrus-session.c:164
msgid "Edit session"
msgstr "Rediger session"

#: ../src/gyrus-session.c:178
msgid "New session"
msgstr "Ny session"

#: ../src/gyrus-session.c:357
msgid "A session name is required."
msgstr "Der kræves et sessionsnavn."

#: ../src/gyrus-session.c:368
#, c-format
msgid "Session named \"%s\" already exists."
msgstr "Sessionen ved navn \"%s\" findes allerede."

#: ../src/gyrus-session.c:464 ../src/gyrus-session.c:598
msgid "Autodetect"
msgstr "Find automatisk"

#: ../src/gyrus-session.c:549
msgid "Session"
msgstr "Session"

#: ../src/gyrus-session.c:718
msgid "No host specified."
msgstr "Ingen vært angivet."

# hvad er dette?
#: ../tests/gyrus-talk.xml.h:1
msgid "Talk - Echo client"
msgstr "Snak - Ekkoklient"

#: ../tests/gyrus-talk.xml.h:5
msgid "_Connect"
msgstr "_Forbind"

#: ../tests/gyrus-talk.xml.h:6
msgid "Connection"
msgstr "Forbindelse"

#: ../tests/gyrus-talk.xml.h:7
msgid "Command:"
msgstr "Kommand:"

#: ../tests/gyrus-talk.xml.h:8
msgid "_Send"
msgstr "_Send"
