# Canadian English translation for Gyrus
# Copyright (C) 2005-2006 Adam Weinberger and the GNOME Foundation
# This file is distributed under the same licence as the gyrus package.
# Adam Weinberger <adamw@gnome.org>, 2005, 2006.
#
#
msgid ""
msgstr ""
"Project-Id-Version: gyrus\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-01-17 20:06-0500\n"
"PO-Revision-Date: 2005-08-27 13:09-0400\n"
"Last-Translator: Adam Weinberger <adamw@gnome.org>\n"
"Language-Team: Canadian English <adamw@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../gyrus.desktop.in.h:1
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr "Administer the mailboxes of your IMAP Cyrus servers"

#: ../gyrus.desktop.in.h:2
msgid "Gyrus IMAP Cyrus Administrator"
msgstr "Gyrus IMAP Cyrus Administrator"

#: ../src/glade/create_mailbox.glade.h:1
msgid "*"
msgstr "*"

#: ../src/glade/create_mailbox.glade.h:2
msgid "<b>Name:</b>"
msgstr "<b>Name:</b>"

#: ../src/glade/create_mailbox.glade.h:3
msgid "<b>Quota (MB):</b>"
msgstr "<b>Quota (MB):</b>"

#: ../src/glade/create_mailbox.glade.h:4
msgid "Assign quota"
msgstr "Assign quota"

#: ../src/glade/create_mailbox.glade.h:5
msgid "Create mailbox"
msgstr "Create mailbox"

#: ../src/glade/find.glade.h:1
msgid "Find"
msgstr "Find"

#: ../src/glade/find.glade.h:2
msgid "Match _entire word only"
msgstr "Match _entire word only"

#: ../src/glade/find.glade.h:3
msgid "Search for:"
msgstr "Search for:"

#: ../src/glade/find.glade.h:4
msgid "_Wrap around"
msgstr "_Wrap around"

#: ../src/glade/page.glade.h:1
msgid "<b>Assigned space:</b>"
msgstr "<b>Assigned space:</b>"

#: ../src/glade/page.glade.h:2
msgid "<b>Enter your password</b>"
msgstr "<b>Enter your password</b>"

#: ../src/glade/page.glade.h:3
msgid "<b>Free space:</b>"
msgstr "<b>Free space:</b>"

#: ../src/glade/page.glade.h:4
msgid "<b>Host:</b>"
msgstr "<b>Host:</b>"

#: ../src/glade/page.glade.h:5
msgid "<b>Owner:</b>"
msgstr "<b>Owner:</b>"

#: ../src/glade/page.glade.h:6
msgid "<b>Port:</b>"
msgstr "<b>Port:</b>"

#: ../src/glade/page.glade.h:7
msgid "<b>User:</b>"
msgstr "<b>User:</b>"

#: ../src/glade/page.glade.h:8
msgid "Access control list"
msgstr "Access control list"

#: ../src/glade/page.glade.h:9
msgid "Modify quota"
msgstr "Modify quota"

#: ../src/glade/page.glade.h:10
msgid "New quota (MB)"
msgstr "New quota (MB)"

#: ../src/glade/page.glade.h:11
msgid "Password"
msgstr "Password"

#: ../src/glade/page.glade.h:12 ../src/gyrus-admin.c:763
#: ../tests/gyrus-talk.glade.h:7
msgid "_Connect"
msgstr "_Connect"

#: ../src/glade/preferences.glade.h:1
msgid "<b>Mailbox quota:</b>"
msgstr "<b>Mailbox quota:</b>"

#: ../src/glade/preferences.glade.h:2
msgid "<b>Mailboxes tree:</b>"
msgstr "<b>Mailboxes tree:</b>"

#: ../src/glade/preferences.glade.h:3
msgid "Default suffix for changing quota:"
msgstr "Default suffix for changing quota:"

#: ../src/glade/preferences.glade.h:4
msgid "Preferences"
msgstr "Preferences"

#: ../src/glade/preferences.glade.h:5
msgid "View complete mailboxes tree"
msgstr "View complete mailboxes tree"

#: ../src/glade/report.glade.h:2
#, no-c-format
msgid "Over (%)"
msgstr "Over (%)"

#: ../src/glade/report.glade.h:3 ../src/gyrus-main-app.c:394
msgid "Report"
msgstr "Report"

#. Translate only Autodetect please.
#: ../src/glade/sessions.glade.h:2
msgid ""
".\n"
"/\n"
"Autodetect"
msgstr ""
".\n"
"/\n"
"Autodetect"

#: ../src/glade/sessions.glade.h:5
msgid "<b>Options</b>"
msgstr "<b>Options</b>"

#: ../src/glade/sessions.glade.h:6
msgid "<b>Session details</b>"
msgstr "<b>Session details</b>"

#: ../src/glade/sessions.glade.h:7
msgid "<small><i><b>Note: </b>Uses a hostname or IP address</i></small>"
msgstr "<small><i><b>Note: </b>Uses a hostname or IP address</i></small>"

#: ../src/glade/sessions.glade.h:8 ../tests/gyrus-talk.glade.h:3
msgid "Host:"
msgstr "Host:"

#: ../src/glade/sessions.glade.h:9
msgid "Mailbox hierarchy separator:"
msgstr "Mailbox hierarchy separator:"

#: ../src/glade/sessions.glade.h:10
msgid "Open session"
msgstr "Open session"

#: ../src/glade/sessions.glade.h:11
msgid "Password:"
msgstr "Password:"

#: ../src/glade/sessions.glade.h:12 ../tests/gyrus-talk.glade.h:4
msgid "Port:"
msgstr "Port:"

#: ../src/glade/sessions.glade.h:13
msgid "Save as:"
msgstr "Save as:"

#: ../src/glade/sessions.glade.h:14 ../tests/gyrus-talk.glade.h:6
msgid "Use a secure connection"
msgstr "Use a secure connection"

#: ../src/glade/sessions.glade.h:15
msgid "Username:"
msgstr "Username:"

#: ../src/gyrus-admin-acl.c:51 ../src/gyrus-admin-acl.c:99
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr "Mailbox '%s' does not exist."

#: ../src/gyrus-admin-acl.c:72
msgid "Invalid identifier."
msgstr "Invalid identifier."

#: ../src/gyrus-admin-acl.c:78
msgid "Empty entry name."
msgstr "Empty entry name."

#: ../src/gyrus-admin-acl.c:83
msgid "Empty mailbox name."
msgstr "Empty mailbox name."

#: ../src/gyrus-admin-acl.c:103
msgid "Missing required argument to Setacl"
msgstr "Missing required argument to Setacl"

#: ../src/gyrus-admin-acl.c:139 ../src/gyrus-admin-mailbox.c:78
msgid "Permission denied"
msgstr "Permission denied"

#: ../src/gyrus-admin-acl.c:171
msgid "Empty access control list."
msgstr "Empty access control list."

#: ../src/gyrus-admin-mailbox.c:76
msgid "Quota does not exist"
msgstr "Quota does not exist"

#: ../src/gyrus-admin-mailbox.c:170
msgid "Quota overloaded"
msgstr "Quota overloaded"

#: ../src/gyrus-admin-mailbox.c:226
msgid "Quota not valid. Please try again."
msgstr "Quota is not valid. Please try again."

#: ../src/gyrus-admin-mailbox.c:240
msgid ""
"Unable to change quota. Are you sure do you have the appropiate permissions?"
msgstr ""
"Unable to change quota. Are you sure you have the appropiate permissions?"

#: ../src/gyrus-admin-mailbox.c:324
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr "'%s' is not a valid mailbox name. Please try a different one."

#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."

#: ../src/gyrus-admin-mailbox.c:342
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr "Mailbox '%s' already exists. Please try a different name."

#: ../src/gyrus-admin-mailbox.c:355
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropiate "
"permissions?"
msgstr ""
"Unable to create the mailbox. Are you sure you have the appropiate "
"permissions?"

#: ../src/gyrus-admin-mailbox.c:365
msgid "Mailbox created, but could not set quota."
msgstr "Mailbox created, but could not set quota."

#: ../src/gyrus-admin-mailbox.c:419
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr "Unable to delete '%s'. Permission denied."

#: ../src/gyrus-admin-mailbox.c:620
msgid "new entry"
msgstr "new entry"

#: ../src/gyrus-admin.c:451 ../src/gyrus-report.c:264
#, c-format
msgid "Users (%d)"
msgstr "Users (%d)"

#: ../src/gyrus-admin.c:469 ../src/gyrus-admin.c:772 ../src/gyrus-report.c:148
msgid "Users"
msgstr "Users"

#: ../src/gyrus-admin.c:539
msgid "lookup"
msgstr "look up"

#: ../src/gyrus-admin.c:540
msgid "read"
msgstr "read"

#: ../src/gyrus-admin.c:541
msgid "seen"
msgstr "seen"

#: ../src/gyrus-admin.c:542
msgid "write"
msgstr "write"

#: ../src/gyrus-admin.c:543
msgid "insert"
msgstr "insert"

#: ../src/gyrus-admin.c:544
msgid "post"
msgstr "post"

#: ../src/gyrus-admin.c:545
msgid "create"
msgstr "create"

#: ../src/gyrus-admin.c:546
msgid "delete"
msgstr "delete"

#: ../src/gyrus-admin.c:547
msgid "admin"
msgstr "admin"

#: ../src/gyrus-admin.c:553
msgid "Identifier"
msgstr "Identifier"

#: ../src/gyrus-admin.c:623
#, c-format
msgid "%s could not be found. Please check the name and try again."
msgstr "%s could not be found. Please check the name and try again."

#: ../src/gyrus-admin.c:634
#, c-format
msgid "Could not connect to %s, port %d."
msgstr "Could not connect to %s, port %d."

#: ../src/gyrus-admin.c:876
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr "Unable to connect with empty passwords. Please enter your password."

#: ../src/gyrus-admin.c:883
msgid "Incorrect login/password"
msgstr "Incorrect login/password"

#: ../src/gyrus-admin.c:891
msgid "_Disconnect"
msgstr "_Disconnect"

#: ../src/gyrus-admin.c:1234
msgid "Could not change permission. Server error: "
msgstr "Could not change permission. Server error: "

#: ../src/gyrus-admin.c:1275
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr "An entry called '%s' already exists. Overwrite it?"

#: ../src/gyrus-dialog-find-mailbox.c:156
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr "The text '%s' was not found in the mailbox list."

#: ../src/gyrus-dialog-find-mailbox.c:257
msgid "Find mailbox"
msgstr "Find mailbox"

#: ../src/gyrus-dialog-mailbox-new.c:80
msgid "Quota not valid"
msgstr "Quota not valid"

#: ../src/gyrus-dialog-mailbox-new.c:236
msgid "New mailbox"
msgstr "New mailbox"

#: ../src/gyrus-main-app.c:129
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr "Really delete mailbox '%s' and all of its submailboxes?"

#: ../src/gyrus-main-app.c:251 ../src/gyrus-main-app.c:426
msgid "Cyrus IMAP Administrator"
msgstr "Cyrus IMAP Administrator"

#: ../src/gyrus-main-app.c:265
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr "%s - Cyrus IMAP Administrator"

#: ../src/gyrus-main-app.c:372
msgid "_File"
msgstr "_File"

#: ../src/gyrus-main-app.c:373
msgid "_Edit"
msgstr "_Edit"

#: ../src/gyrus-main-app.c:374
msgid "_ACL"
msgstr "_ACL"

#: ../src/gyrus-main-app.c:375
msgid "_View"
msgstr "_View"

#: ../src/gyrus-main-app.c:376
msgid "_Help"
msgstr "_Help"

#: ../src/gyrus-main-app.c:377
msgid "Go to server..."
msgstr "Go to server..."

#: ../src/gyrus-main-app.c:378
msgid "Show the list of servers"
msgstr "Show the list of servers"

#: ../src/gyrus-main-app.c:379
msgid "_Quit"
msgstr "_Quit"

#: ../src/gyrus-main-app.c:380
msgid "Quit the program"
msgstr "Quit the program"

#: ../src/gyrus-main-app.c:381
msgid "_About"
msgstr "_About"

#: ../src/gyrus-main-app.c:388
msgid "Add mailbox"
msgstr "Add mailbox"

#: ../src/gyrus-main-app.c:389
msgid "Add a mailbox under the one selected"
msgstr "Add a mailbox under the one selected"

#: ../src/gyrus-main-app.c:390
msgid "_Find"
msgstr "_Find"

#: ../src/gyrus-main-app.c:391
msgid "Search for a mailbox in current server"
msgstr "Search for a mailbox in current server"

#: ../src/gyrus-main-app.c:392
msgid "_Refresh"
msgstr "_Refresh"

#: ../src/gyrus-main-app.c:393
msgid "Refresh the mailbox list"
msgstr "Refresh the mailbox list"

#: ../src/gyrus-main-app.c:395
msgid "Create report of users with quota problems"
msgstr "Create report of users with quota problems"

#: ../src/gyrus-main-app.c:399
msgid "New entry"
msgstr "New entry"

#: ../src/gyrus-main-app.c:400
msgid "Create a new ACL entry in current mailbox"
msgstr "Create a new ACL entry in current mailbox"

#: ../src/gyrus-main-app.c:401
msgid "Remove mailbox"
msgstr "Remove mailbox"

#: ../src/gyrus-main-app.c:402
msgid "Remove current mailbox from the server"
msgstr "Remove current mailbox from the server"

#: ../src/gyrus-main-app.c:407
msgid "Rename entry"
msgstr "Rename entry"

#: ../src/gyrus-main-app.c:408
msgid "Rename selected ACL entry"
msgstr "Rename selected ACL entry"

#: ../src/gyrus-main-app.c:409
msgid "Delete entry"
msgstr "Delete entry"

#: ../src/gyrus-main-app.c:410
msgid "Delete selected ACL entry"
msgstr "Delete selected ACL entry"

#: ../src/gyrus-main-app.c:542
msgid "translators-credits"
msgstr "Adam Weinberger <adamw@gnome.org>"

#: ../src/gyrus-main-app.c:552
msgid "GNOME Cyrus Administrator"
msgstr "GNOME Cyrus Administrator"

#: ../src/gyrus-main-app.c:554
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"

#: ../src/gyrus-main-app.c:556
msgid "Administration tool for Cyrus IMAP servers."
msgstr "Administration tool for Cyrus IMAP servers."

#. set title
#: ../src/gyrus-report.c:84
#, c-format
msgid "Report: %s"
msgstr "Report: %s"

#: ../src/gyrus-report.c:169 ../src/gyrus-report.c:456
msgid "Quota (%)"
msgstr "Quota (%)"

#: ../src/gyrus-report.c:182 ../src/gyrus-report.c:459
msgid "Assigned (KB)"
msgstr "Assigned (KB)"

#: ../src/gyrus-report.c:193 ../src/gyrus-report.c:462
msgid "Used (KB)"
msgstr "Used (KB)"

#: ../src/gyrus-report.c:382
msgid "Print Report"
msgstr "Print Report"

#. /my_status_bar_print ("Print previewing ...");
#: ../src/gyrus-report.c:425
msgid "Preview"
msgstr "Preview"

#: ../src/gyrus-report.c:453
msgid "User"
msgstr "User"

#: ../src/gyrus-session.c:163
msgid "Edit session"
msgstr "Edit session"

#: ../src/gyrus-session.c:177
msgid "New session"
msgstr "New session"

#: ../src/gyrus-session.c:339
msgid "A session name is required."
msgstr "A session name is required."

#: ../src/gyrus-session.c:350
#, c-format
msgid "Session named \"%s\" already exists."
msgstr "Session named \"%s\" already exists."

#: ../src/gyrus-session.c:388
msgid "Session saved"
msgstr "Session saved"

#: ../src/gyrus-session.c:449
msgid "Autodetect"
msgstr "Autodetect"

#: ../src/gyrus-session.c:539
msgid "Session"
msgstr "Session"

#: ../src/gyrus-session.c:690
msgid "No host specified."
msgstr "No host specified."

#: ../tests/gyrus-talk.glade.h:1
msgid "<b>Connection</b>"
msgstr "<b>Connection</b>"

#: ../tests/gyrus-talk.glade.h:2
msgid "Command:"
msgstr "Command:"

#: ../tests/gyrus-talk.glade.h:5
msgid "Talk - Echo client"
msgstr "Talk - Echo client"

#: ../tests/gyrus-talk.glade.h:8
msgid "_Send"
msgstr "_Send"
