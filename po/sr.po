# Serbian translation for gyrus.
# Copyright (C) 2013 gyrus's COPYRIGHT HOLDER
# This file is distributed under the same license as the gyrus package.
# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: gyrus master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gyrus&"
"keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-11-12 01:22+0000\n"
"PO-Revision-Date: 2013-01-15 09:44+0200\n"
"Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>\n"
"Language-Team: Serbian <gnom@prevod.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : "
"n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Project-Style: gnome\n"

#: ../gyrus.desktop.in.in.h:1
msgid "Gyrus IMAP Cyrus Administrator"
msgstr "Гирус ИМАП Цирус администратор"

#: ../gyrus.desktop.in.in.h:2
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr "Администрирајте сандучиће вашег ИМАП Цирус сервера"

#: ../src/ui/create_mailbox.xml.h:1
msgid "Name:"
msgstr "Име:"

#: ../src/ui/create_mailbox.xml.h:2
msgid "Quota (MB):"
msgstr "Квота (MB):"

#: ../src/ui/create_mailbox.xml.h:3
msgid "Assign quota"
msgstr "Доделите квоту"

#: ../src/ui/find.xml.h:1
msgid "Search for:"
msgstr "Потражи:"

#: ../src/ui/find.xml.h:2
msgid "Match _entire word only"
msgstr "Упореди само _целе речи"

#: ../src/ui/find.xml.h:3
msgid "_Wrap around"
msgstr "_Преламај около"

#: ../src/ui/page.xml.h:1
msgid "Free space:"
msgstr "Слободан простор:"

#: ../src/ui/page.xml.h:2
msgid "Assigned space:"
msgstr "Додељени простор:"

#: ../src/ui/page.xml.h:3
msgid "Owner:"
msgstr "Власник:"

#: ../src/ui/page.xml.h:4
msgid "New quota (MB)"
msgstr "Нова квота (MB)"

#: ../src/ui/page.xml.h:5
msgid "Modify quota"
msgstr "Измените квоту"

#: ../src/ui/page.xml.h:6
msgid "Access control list"
msgstr "Списак управљања приступом"

#: ../src/ui/page.xml.h:7 ../src/ui/sessions_edit.xml.h:5
#: ../tests/gyrus-talk.xml.h:2
msgid "Host:"
msgstr "Домаћин:"

#: ../src/ui/page.xml.h:8
msgid "User:"
msgstr "Корисник:"

#: ../src/ui/page.xml.h:9 ../src/ui/sessions_edit.xml.h:4
#: ../tests/gyrus-talk.xml.h:4
msgid "Port:"
msgstr "Прикључник:"

#: ../src/ui/password.xml.h:1
msgid "Password"
msgstr "Лозинка"

#: ../src/ui/password.xml.h:2
msgid "Enter your password"
msgstr "Унесите вашу лозинку"

#: ../src/ui/report.xml.h:1
msgid "Report"
msgstr "Извештај"

#: ../src/ui/report.xml.h:3
#, no-c-format
msgid "Over (%)"
msgstr "Преко (%)"

#: ../src/ui/sessions.xml.h:1
msgid "Open session"
msgstr "Отворите сесију"

#: ../src/ui/sessions_edit.xml.h:1
msgid "Session name:"
msgstr "Назив сесије:"

#: ../src/ui/sessions_edit.xml.h:2
msgid "Password:"
msgstr "Лозинка:"

#: ../src/ui/sessions_edit.xml.h:3
msgid "Username:"
msgstr "Корисничко име:"

#: ../src/ui/sessions_edit.xml.h:6
msgid "Session details"
msgstr "Појединости сесије"

#: ../src/ui/sessions_edit.xml.h:7 ../tests/gyrus-talk.xml.h:3
msgid "Use a secure connection"
msgstr "Користи безбедну везу"

#: ../src/ui/sessions_edit.xml.h:8
msgid "Mailbox hierarchy separator:"
msgstr "Раздвојник редоследа сандучића:"

#: ../src/ui/sessions_edit.xml.h:9
msgid "<b>Options</b>"
msgstr "<b>Могућности</b>"

#: ../src/gyrus-admin-acl.c:54 ../src/gyrus-admin-acl.c:103
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr "Сандуче „%s“ не постоји."

#: ../src/gyrus-admin-acl.c:76
msgid "Invalid identifier."
msgstr "Неисправан одредник."

#: ../src/gyrus-admin-acl.c:82
msgid "Empty entry name."
msgstr "Назив уноса је празан."

#: ../src/gyrus-admin-acl.c:87
msgid "Empty mailbox name."
msgstr "Назив сандучета је празан."

#: ../src/gyrus-admin-acl.c:107
msgid "Missing required argument to Setacl"
msgstr "Недостаје потребан аргумент за подешавање списка управљања приступом"

#: ../src/gyrus-admin-acl.c:143 ../src/gyrus-admin-mailbox.c:80
msgid "Permission denied"
msgstr "Приступ је одбијен"

#: ../src/gyrus-admin-acl.c:175
msgid "Empty access control list."
msgstr "Празан списак управљања приступом."

#: ../src/gyrus-admin-mailbox.c:78
msgid "Quota does not exist"
msgstr "Квота не постоји"

#: ../src/gyrus-admin-mailbox.c:172
#, c-format
msgid "Quota overloaded"
msgstr "Квота је премашена"

#: ../src/gyrus-admin-mailbox.c:230
msgid "Quota not valid. Please try again."
msgstr "Квота није исправна. Молим покушајте поново."

#: ../src/gyrus-admin-mailbox.c:244
msgid ""
"Unable to change quota. Are you sure do you have the appropriate permissions?"
msgstr ""
"Не могу да изменим квоту. Да ли сте сигурни да имате одговарајућа овлашћења?"

#: ../src/gyrus-admin-mailbox.c:324
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr "„%s“ није исправан назив сандучета. Молим покушајте неко друго."

#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr ""
"Полазно сандуче „%s“ не постоји. Молим освежите списак сандучића и покушајте "
"опет."

#: ../src/gyrus-admin-mailbox.c:342
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr "Сандуче „%s“ већ постоји. Молим покушајте други назив."

#: ../src/gyrus-admin-mailbox.c:355
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropriate "
"permissions?"
msgstr ""
"Не могу да направим сандуче. Да ли сте сигурни да имате одговарајућа "
"овлашћења?"

#: ../src/gyrus-admin-mailbox.c:366
msgid "Mailbox created, but could not set quota."
msgstr "Направио сам сандуче, али не могу да подесим квоту."

#: ../src/gyrus-admin-mailbox.c:433
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr "Не могу да обришем „%s“. Овлашћење је одбијено."

#: ../src/gyrus-admin-mailbox.c:634
msgid "new entry"
msgstr "нови унос"

#: ../src/gyrus-admin.c:447 ../src/gyrus-report.c:289
#, c-format
msgid "Users (%d)"
msgstr "Корисници (%d)"

#: ../src/gyrus-admin.c:453
#, c-format
msgid "Orphaned mailboxes (%d)"
msgstr "Напуштени сандучићи (%d)"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:855
msgid "Orphaned mailboxes"
msgstr "Напуштени сандучићи"

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:853 ../src/gyrus-report.c:170
msgid "Users"
msgstr "Корисници"

#: ../src/gyrus-admin.c:565
msgid "lookup"
msgstr "претражује"

#: ../src/gyrus-admin.c:566
msgid "read"
msgstr "чита"

#: ../src/gyrus-admin.c:567
msgid "seen"
msgstr "гледа"

#: ../src/gyrus-admin.c:568
msgid "write"
msgstr "пише"

#: ../src/gyrus-admin.c:569
msgid "insert"
msgstr "умеће"

#: ../src/gyrus-admin.c:570
msgid "post"
msgstr "уписује"

#: ../src/gyrus-admin.c:571
msgid "create"
msgstr "ствара"

#: ../src/gyrus-admin.c:572
msgid "delete"
msgstr "брише"

#: ../src/gyrus-admin.c:573
msgid "admin"
msgstr "администрира"

#: ../src/gyrus-admin.c:581
msgid "Identifier"
msgstr "Одредник"

#: ../src/gyrus-admin.c:651
msgid "Couldn't create the client socket."
msgstr "Не могу да направим утичницу клијента."

#: ../src/gyrus-admin.c:659
msgid "Couldn't parse the server address."
msgstr "Не могу да обрадим адресу сервера."

#: ../src/gyrus-admin.c:668
#, c-format
msgid "Could not connect to %s, port %d."
msgstr "Не могу да се повежем на „%s“, прикључник %d."

#: ../src/gyrus-admin.c:1001
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr "Не могу да се повежем са празном лозинком. Молим упишите вашу лозинку."

#: ../src/gyrus-admin.c:1008
msgid "Incorrect login/password"
msgstr "Неисправна пријава/лозинка"

#: ../src/gyrus-admin.c:1364
msgid "Could not change permission. Server error: "
msgstr "Не могу да изменим овлашћења. Грешка сервера: "

#: ../src/gyrus-admin.c:1407
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr "Унос под називом „%s“ већ постоји. Да га препишем?"

#: ../src/gyrus-dialog-find-mailbox.c:171
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr "Нисам пронашао текст „%s“ на списку сандучића."

#: ../src/gyrus-dialog-find-mailbox.c:275
msgid "Find mailbox"
msgstr "Нађи сандуче"

#: ../src/gyrus-dialog-mailbox-new.c:93
msgid "Quota not valid"
msgstr "Квота није исправна"

#: ../src/gyrus-dialog-mailbox-new.c:220
msgid "New mailbox"
msgstr "Ново сандуче"

#: ../src/gyrus-main-app.c:146
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr "Да обришем сандуче „%s“ и све његове садржане сандучиће?"

#: ../src/gyrus-main-app.c:261 ../src/gyrus-main-app.c:436
#: ../src/gyrus-main-app.c:732
msgid "Cyrus IMAP Administrator"
msgstr "Цирус ИМАП администратор"

#: ../src/gyrus-main-app.c:275
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr "%s — Цирус ИМАП администратор"

#: ../src/gyrus-main-app.c:383
msgid "_File"
msgstr "_Датотека"

#: ../src/gyrus-main-app.c:384
msgid "_Edit"
msgstr "_Уређивање"

#: ../src/gyrus-main-app.c:385
msgid "_ACL"
msgstr "_СУП"

#: ../src/gyrus-main-app.c:386
msgid "_View"
msgstr "_Преглед"

#: ../src/gyrus-main-app.c:387
msgid "_Help"
msgstr "По_моћ"

#: ../src/gyrus-main-app.c:388
msgid "Go to server..."
msgstr "Иди на сервер..."

#: ../src/gyrus-main-app.c:389
msgid "Show the list of servers"
msgstr "Прикажите списак сервера"

#: ../src/gyrus-main-app.c:397
msgid "Add mailbox"
msgstr "Додај сандуче"

#: ../src/gyrus-main-app.c:398
msgid "Add a mailbox under the one selected"
msgstr "Додај сандуче под једним изабраним"

#: ../src/gyrus-main-app.c:400
msgid "Search for a mailbox in current server"
msgstr "Потражи сандуче на текућем серверу"

#: ../src/gyrus-main-app.c:402
msgid "Refresh the mailbox list"
msgstr "Освежи списак сандучића"

#: ../src/gyrus-main-app.c:403
msgid "Create report..."
msgstr "Направи извештај..."

#: ../src/gyrus-main-app.c:404
msgid "Create report of users with quota problems"
msgstr "Направите извештај о корисницима са проблемима са квотом"

#: ../src/gyrus-main-app.c:408
msgid "New entry"
msgstr "Нови унос"

#: ../src/gyrus-main-app.c:409
msgid "Create a new ACL entry in current mailbox"
msgstr "Направите нови СУП унос у текућем сандучету"

#: ../src/gyrus-main-app.c:410
msgid "Remove mailbox"
msgstr "Уклони сандуче"

#: ../src/gyrus-main-app.c:411
msgid "Remove current mailbox from the server"
msgstr "Уклоните текуће сандуче са сервера"

#: ../src/gyrus-main-app.c:416
msgid "Rename entry"
msgstr "Преименуј унос"

#: ../src/gyrus-main-app.c:417
msgid "Rename selected ACL entry"
msgstr "Преименујте изабрани СУП унос"

#: ../src/gyrus-main-app.c:418
msgid "Delete entry"
msgstr "Обриши унос"

#: ../src/gyrus-main-app.c:419
msgid "Delete selected ACL entry"
msgstr "Обришите изабрани СУП унос"

#: ../src/gyrus-main-app.c:552
msgid "translators-credits"
msgstr ""
"  Мирослав Николић\n"
"\n"
"  http://prevod.org — превод на српски језик"

#: ../src/gyrus-main-app.c:562
msgid "GNOME Cyrus Administrator"
msgstr "Гномов Цирус администратор"

#: ../src/gyrus-main-app.c:564
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""
"(c) 2003-2005 Гномова задужбина\n"
"(c) 2004-2005 Клаудио Саведра"

#: ../src/gyrus-main-app.c:566
msgid "Administration tool for Cyrus IMAP servers."
msgstr "Алат за администрацију Цирус ИМАП сервера."

#. set title
#: ../src/gyrus-report.c:107
#, c-format
msgid "Mailbox space usage report for %s"
msgstr "Извештај о коришћењу простора сандучета за %s"

#: ../src/gyrus-report.c:191 ../src/gyrus-report.c:492
msgid "Quota (%)"
msgstr "Квота (%)"

#: ../src/gyrus-report.c:204 ../src/gyrus-report.c:496
msgid "Assigned (KB)"
msgstr "Додељено (KB)"

#: ../src/gyrus-report.c:215 ../src/gyrus-report.c:500
msgid "Used (KB)"
msgstr "Искоришћено (KB)"

#. Translators: this represents the number of pages being printed.
#: ../src/gyrus-report.c:465
#, c-format
msgid "%d/%d"
msgstr "%d/%d"

#: ../src/gyrus-report.c:488
msgid "User"
msgstr "Корисник"

#: ../src/gyrus-session.c:164
msgid "Edit session"
msgstr "Уреди сесију"

#: ../src/gyrus-session.c:178
msgid "New session"
msgstr "Нова сесија"

#: ../src/gyrus-session.c:357
msgid "A session name is required."
msgstr "Потребан је назив сесије."

#: ../src/gyrus-session.c:368
#, c-format
msgid "Session named \"%s\" already exists."
msgstr "Већ постоји сесија „%s“."

#: ../src/gyrus-session.c:464 ../src/gyrus-session.c:598
msgid "Autodetect"
msgstr "Сам препознај"

#: ../src/gyrus-session.c:549
msgid "Session"
msgstr "Сесија"

#: ../src/gyrus-session.c:718
msgid "No host specified."
msgstr "Није наведен домаћин."

#: ../tests/gyrus-talk.xml.h:1
msgid "Talk - Echo client"
msgstr "Ток — Клијент одјека"

#: ../tests/gyrus-talk.xml.h:5
msgid "_Connect"
msgstr "_Повежи се"

#: ../tests/gyrus-talk.xml.h:6
msgid "Connection"
msgstr "Веза"

#: ../tests/gyrus-talk.xml.h:7
msgid "Command:"
msgstr "Наредба:"

#: ../tests/gyrus-talk.xml.h:8
msgid "_Send"
msgstr "_Пошаљи"
