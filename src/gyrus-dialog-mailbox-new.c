/*
  gyrus-dialog-mailbox-new.c

  GYRUS -- GNOME Cyrus Administrator. Dialog New Mailbox.

  Copyright (C) 2004 Claudio Saavedra Valdés <csaavedra@alumnos.utalca.cl>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#include <config.h>

#include <gtk/gtk.h>
#include <glib-object.h>

#include "glib/gi18n.h"
#include "gyrus-common.h"
#include "gyrus-dialog-mailbox-new.h"
#include "gyrus-main-app.h"
#include "gyrus-admin.h"
#include "gyrus-admin-mailbox.h"

struct _GyrusDialogMailboxNewPrivate {
	GtkWidget *entry_mailbox;
	GtkWidget *entry_quota;

	GtkWidget *label_mailbox;
	GtkWidget *label_quota;
	
	GtkWidget *checkbutton_quota;

	GtkWidget *button_ok;
	GtkWidget *button_cancel;
};

G_DEFINE_TYPE_WITH_PRIVATE (GyrusDialogMailboxNew, gyrus_dialog_mailbox_new,
                            GTK_TYPE_DIALOG);

/* Callbacks */

static void
gyrus_dialog_mailbox_new_on_button_ok_clicked (GtkButton *button,
					       gpointer user_data)
{
	GyrusDialogMailboxNew *dialog;
	GyrusMainApp *app;
	gchar *current_path;
	const gchar *mailbox;
	gint quota;
	gchar *error;
	gchar *quota_invalid;
	GyrusAdmin *admin;

	dialog = GYRUS_DIALOG_MAILBOX_NEW (user_data);
	app = GYRUS_MAIN_APP (g_application_get_default ());
	
	admin = gyrus_main_app_get_current_admin (app);
	
	current_path = gyrus_admin_get_selected_mailbox (admin);

	if (!current_path)
		current_path = g_strdup ("user");

	mailbox = gtk_entry_get_text (GTK_ENTRY (dialog->priv->entry_mailbox));

	if (gtk_toggle_button_get_active
	    (GTK_TOGGLE_BUTTON (dialog->priv->checkbutton_quota))) {
		quota = 1024 * g_strtod (gtk_entry_get_text
				  (GTK_ENTRY (dialog->priv->entry_quota)),
				  &quota_invalid);
	} else
		quota = -1;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON
					  (dialog->priv->checkbutton_quota)) &&
	    *quota_invalid) {
		gyrus_common_show_message (NULL, GTK_MESSAGE_ERROR,
					   _("Quota not valid"));
		
		gtk_editable_select_region (GTK_EDITABLE
					    (dialog->priv->entry_quota),
					    0, -1);
	}
	else if (gyrus_admin_mailbox_new (admin, mailbox,
					  current_path, quota, &error)) {
		gyrus_admin_refresh_users_list (admin);
		gyrus_admin_select_mailbox (admin, current_path);

		gtk_widget_destroy (GTK_WIDGET (dialog));
		
	}
	else {
		gyrus_common_show_message (GTK_WINDOW (dialog),
					   GTK_MESSAGE_ERROR, error);
		gtk_editable_select_region (GTK_EDITABLE (dialog->priv->entry_mailbox),
					    0, -1);
		g_free (error);
	}
	
	g_free (current_path);
}

static void
gyrus_dialog_mailbox_new_on_checkbutton_toggled (GtkToggleButton *toggle,
						 gpointer user_data)
{
	GyrusDialogMailboxNew *dialog = GYRUS_DIALOG_MAILBOX_NEW (user_data);
	
	gboolean sensitive = gtk_toggle_button_get_active (toggle);
	
	gtk_widget_set_sensitive (dialog->priv->label_quota, sensitive);
	gtk_widget_set_sensitive (dialog->priv->entry_quota, sensitive);

	sensitive =
		gyrus_gtk_entry_has_text (GTK_ENTRY (dialog->priv->entry_mailbox)) &&
		(!gtk_toggle_button_get_active
		 (GTK_TOGGLE_BUTTON (dialog->priv->checkbutton_quota)) ||
		 gyrus_gtk_entry_has_text (GTK_ENTRY (dialog->priv->entry_quota)));

	gtk_widget_set_sensitive (dialog->priv->button_ok, sensitive);
}

static void
gyrus_dialog_mailbox_new_on_entry_changed (GtkEditable *editable,
					   gpointer user_data)
{
	GyrusDialogMailboxNew *dialog = GYRUS_DIALOG_MAILBOX_NEW (user_data);

	gboolean sensitive = gyrus_gtk_entry_has_text (GTK_ENTRY (dialog->priv->entry_mailbox)) &&
		(!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->priv->checkbutton_quota)) ||
		 gyrus_gtk_entry_has_text (GTK_ENTRY (dialog->priv->entry_quota)));
	
	gtk_widget_set_sensitive (dialog->priv->button_ok, sensitive);
}

static void
gyrus_dialog_mailbox_new_class_init (GyrusDialogMailboxNewClass *class)
{
	gyrus_dialog_mailbox_new_parent_class = g_type_class_peek_parent (class);
}

static void
gyrus_dialog_mailbox_new_init_get_widgets (GyrusDialogMailboxNew *dialog)
{
	GtkWidget *table;
	GtkBuilder *builder;

        builder = gtk_builder_new ();
        gtk_builder_add_from_resource (builder, "/org/gnome/cyrus/create_mailbox.xml", NULL);

	table = GTK_WIDGET (gtk_builder_get_object (builder, "table_dialog"));
	gtk_container_add (GTK_CONTAINER (gtk_dialog_get_content_area
	                                  (GTK_DIALOG (dialog))), table);
	
	dialog->priv->button_cancel
	        = gtk_dialog_add_button (GTK_DIALOG (dialog), _("_Close"),
	                                 GTK_RESPONSE_CANCEL);
	dialog->priv->button_ok = gtk_dialog_add_button (GTK_DIALOG (dialog),
	                                                 _("_OK"),
	                                                 GTK_RESPONSE_ACCEPT);

	dialog->priv->checkbutton_quota = GTK_WIDGET (gtk_builder_get_object (builder, "checkbutton_quota"));
	dialog->priv->entry_mailbox = GTK_WIDGET (gtk_builder_get_object (builder, "entry_mailbox"));
	dialog->priv->entry_quota = GTK_WIDGET (gtk_builder_get_object (builder, "entry_quota"));
	dialog->priv->label_mailbox = GTK_WIDGET (gtk_builder_get_object (builder, "label_mailbox"));
	dialog->priv->label_quota = GTK_WIDGET (gtk_builder_get_object (builder, "label_quota"));

	g_object_unref (builder);
}

static void
gyrus_dialog_mailbox_new_init_connect_signals (GyrusDialogMailboxNew *dialog)
{
	g_signal_connect (G_OBJECT (dialog->priv->button_ok), "clicked",
			  G_CALLBACK (gyrus_dialog_mailbox_new_on_button_ok_clicked),
			  dialog);

	g_signal_connect (G_OBJECT (dialog->priv->checkbutton_quota), "toggled",
			  G_CALLBACK (gyrus_dialog_mailbox_new_on_checkbutton_toggled),
			  dialog);

	g_signal_connect_swapped (G_OBJECT (dialog->priv->button_cancel), "clicked",
				  G_CALLBACK (gtk_widget_destroy), dialog);

	g_signal_connect (G_OBJECT (dialog->priv->entry_mailbox), "changed",
			  G_CALLBACK (gyrus_dialog_mailbox_new_on_entry_changed),
			  dialog);
	
	g_signal_connect (G_OBJECT (dialog->priv->entry_quota), "changed",
			  G_CALLBACK (gyrus_dialog_mailbox_new_on_entry_changed),
			  dialog);
}

static void
gyrus_dialog_mailbox_new_init (GyrusDialogMailboxNew *dialog)
{
	dialog->priv = gyrus_dialog_mailbox_new_get_instance_private (dialog);

	gtk_window_set_titlebar (GTK_WINDOW (dialog), NULL);
	gtk_window_set_title (GTK_WINDOW (dialog), _("New mailbox"));
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	
	gyrus_dialog_mailbox_new_init_get_widgets (dialog);
	gyrus_dialog_mailbox_new_init_connect_signals (dialog);
	gtk_widget_set_sensitive (dialog->priv->button_ok, FALSE);
	gtk_widget_show_all (GTK_WIDGET (dialog));
}

GtkWidget *
gyrus_dialog_mailbox_new_new (void)
{
	GyrusDialogMailboxNew *dialog;
	dialog = g_object_new (GYRUS_TYPE_DIALOG_MAILBOX_NEW, NULL);
	return GTK_WIDGET (dialog);
}
