/*
  gyrus-admin-acl.h

  GYRUS -- GNOME Cyrus Administrator. ACL support.
  
  Copyright (C) 2004 Claudio Saavedra V. <csaavedra@alumnos.utalca.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GYRUS_ADMIN_ACL_H
#define GYRUS_ADMIN_ACL_H

typedef struct {
	gchar *identifier;
	gchar *rights;
} GyrusImapAclEntry;

#define GYRUS_IMAP_ACL_ENTRY(a) ((GyrusImapAclEntry*)(a))

gboolean
gyrus_admin_acl_set_entry (GyrusAdmin *admin, const gchar *mailbox,
			   const gchar* user, const gchar* permissions,
			   gchar **error);

/* Returns a GList with GyrusImapAclEntry elements.
   The list must be freed, and also it's elements.
*/
GList *
gyrus_admin_acl_get (GyrusAdmin *admin, const gchar *mailbox,
		     gchar **error);

/*
 * Free the list created with gyrus_admin_acl_get
 */
void
gyrus_admin_acl_list_free (GList *list);


/* Deletes the entry of @identifier in @mailbox. If something goes
 * wrong returns FALSE and allocate a error message in @error.
 */
gboolean
gyrus_admin_acl_delete_entry (GyrusAdmin *admin, const gchar *mailbox,
			      const gchar *identifier, gchar **error);

#endif /* GYRUS_ADMIN_ACL_H */
