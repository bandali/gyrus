/*
  gyrus-dialog-find-mailbox.h

  GYRUS -- GNOME Cyrus Administrator. Find mailbox dialog and engine.
  
  Copyright (C) 2004 Claudio Saavedra V. <csaavedra@alumnos.utalca.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GYRUS_DIALOG_FIND_MAILBOX_H

#include "gyrus-admin.h"

#define GYRUS_DIALOG_FIND_MAILBOX_H

#define GYRUS_TYPE_DIALOG_FIND_MAILBOX            (gyrus_dialog_find_mailbox_get_type ())
#define GYRUS_DIALOG_FIND_MAILBOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GYRUS_TYPE_DIALOG_FIND_MAILBOX, GyrusDialogFindMailbox))
#define GYRUS_DIALOG_FIND_MAILBOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GYRUS_TYPE_DIALOG_FIND_MAILBOX, GyrusDialogFindMailboxClass))
#define GYRUS_IS_DIALOG_FIND_MAILBOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GYRUS_TYPE_DIALOG_FIND_MAILBOX))
#define GYRUS_IS_DIALOG_FIND_MAILBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GYRUS_TYPE_DIALOG_FIND_MAILBOX))
#define GYRUS_DIALOG_FIND_MAILBOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GYRUS_TYPE_DIALOG_FIND_MAILBOX, GyrusDialogFindMailbox))

typedef struct _GyrusDialogFindMailboxPrivate GyrusDialogFindMailboxPrivate;
typedef struct _GyrusDialogFindMailbox        GyrusDialogFindMailbox;
typedef struct _GyrusDialogFindMailboxClass   GyrusDialogFindMailboxClass;

struct _GyrusDialogFindMailbox  {
	GtkDialog dialog;
	GyrusDialogFindMailboxPrivate *priv;
};

struct _GyrusDialogFindMailboxClass {
	 GtkDialogClass dialog_class;
};

GType gyrus_dialog_find_mailbox_get_type (void);

/** 
    Create a dialog to search mailboxes.
*/
GtkWidget *gyrus_dialog_find_mailbox_new (void);

/**
   Sets the administrator where the dialog should
   look for the requested mailboxes.
*/
void
gyrus_dialog_find_mailbox_set_admin (GyrusDialogFindMailbox *dialog,
				     GyrusAdmin *admin);

#endif /* GYRUS_DIALOG_FIND_MAILBOX_H */
