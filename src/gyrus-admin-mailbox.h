/*
  gyrus-admin-mailbox.h

  GYRUS -- GNOME Cyrus Administrator. Administrator Mailboxes Modules.

  Copyright (C) 2003-2004 Alejandro Valdés Jiménez <avaldes@utalca.cl>
  Copyright (C) 2003-2004 Jorge Bustos Bustos <jbustos@utalca.cl>
  Copyright (C) 2003-2004 Claudio Saavedra Valdés <csaavedra@alumnos.utalca.cl>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#ifndef GYRUS_ADMIN_MAILBOX_H
#define GYRUS_ADMIN_MAILBOX_H

/**
   Gets the Quota of @mailbox_path in @admin. A Quota is composed
   of two values, the limit of the mailbox and the currently used 
   space. Both values are in KBytes.

   If @mailbox_path does not have a quota defined, then
   both @quota_limit and @quota_used are set to -1.
   
   @admin: A @GyrusAdmin instance.
   @mailbox_path: The path to the mailbox to check 
   for the quota.
   @quota_limit: The limit of space of the mailbox for the 
   mailbox.
   @quota_used: The currently used space.
   @error: If non NULL, a error message of the server, 
   if any.

   Returns: TRUE on all ok. If anything goes wrong returns FALSE
   and allocates a error message in @error.

*/
gboolean
gyrus_admin_mailbox_get_quota (GyrusAdmin *admin, const gchar *mailbox_path,
			       gint *quota_limit, gint *quota_used, gchar **message);

/**
   Sets the quota of @mailbox_path in @admin to @new_quota. @new_quota
   is in KBytes.
*/
gboolean
gyrus_admin_mailbox_set_quota (GyrusAdmin *admin, const gchar *mailbox_path,
			       gint new_quota);

/**
   When a mailbox is selected in the treeview, some usefull information
   is displayed by gyrus_admin_show_info (). 
   Use this function to clear that information.
*/
void
gyrus_admin_mailbox_clear_info (GyrusAdmin *admin);

/**
   Shows usefull information of @mailbox_path in @admin. 

   @admin: A instance of #GyrusAdmin.
   @user: The user that owns @mailbox_path.
   @mailbox_path: The path to the mailbox whose info is to
   be shown.

*/
void
gyrus_admin_mailbox_show_info (GyrusAdmin *admin, const gchar *user,
			       const gchar *mailbox_path);

/**
   Updates the sensitivity of the items related to the Mailbox.
   Useful when user selects/deselects a mailbox row in the Treeview.
*/
void
gyrus_admin_mailbox_set_sensitive (GyrusAdmin *admin, gboolean status);

/**
   Creates a mailbox in the server.

   This function will create @mailbox in the given @path, setting a
   maximun storage space of @quota. If @quota is negative, doesn't
   set a quota.

   @admin: A instance of #GyrusAdmin. 
   @mailbox: The mailbox name to create.
   @path: The path where to create the mailbox.
   @quota: If non negative, the maximun storage space for the mailbox in KB.
   @error: If non NULL, allocates a error message.

   Returns: TRUE if sucessfull, else returns FALSE and allocates 
   and error message in @error.
*/
gboolean
gyrus_admin_mailbox_new (GyrusAdmin *admin, const gchar *mailbox,
			 const gchar *path, gint quota, gchar **error);

/**
   Deletes a mailbox and recursivly deletes all of its 
   submailboxes.

   If the user administrator doesn't have the permission 'd', 
   doesn't delete the mailboxes.

   @admin: An instance of #GyrusAdmin.
   @mailbox: The mailbox to delete.
  
 */
void
gyrus_admin_mailbox_delete_all (GyrusAdmin *admin, const gchar *mailbox);

/**
  Deletes the selected entry of the ACL.

  The selected entry is the selected row in the ACL TreeView.

  @admin: An instance of #GyrusAdmin.
*/
void
gyrus_admin_delete_selected_acl_entry (GyrusAdmin *admin);

/**
   Starts editing the selected ACL entry name in the ACL TreeView.

   The selected entry is the selected row in the ACL TreeView.

   @admin: An instance of #GyrusAdmin.
*/
void
gyrus_admin_start_editing_selected_acl (GyrusAdmin *admin);

/**
   Checks if the ACL TreeView has any element selected.

   @admin: An instance of #GyrusAdmin.

   Returns: TRUE if any row is selected, FALSE otherwise.
*/
gboolean
gyrus_admin_acl_has_selection (GyrusAdmin *admin);

/**
   Adds a row in the ACL TreeView, so user can start editing the 
   name of the entry. The entry will have a default name acording
   to the locale.

   @admin: An instance of #GyrusAdmin.
*/
void
gyrus_admin_add_acl_entry (GyrusAdmin *admin);

/* Callbacks */
void
gyrus_admin_mailbox_on_button_quota_apply_clicked (GtkButton *button,
						   gpointer userdata);
void
gyrus_admin_mailbox_on_entry_quota_new_activate (GtkEntry *entry,
						 gpointer user_data);

#endif /* GYRUS_ADMIN_MAILBOX_H */
