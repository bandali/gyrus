/*
  gyrus-connection.h - Abstract connection object with TLS support

  GYRUS -- GNOME Cyrus Administrator.
  
  Copyright (C) 2005 Mario Fuentes <mario@gnome.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GYRUS_CONNECTION_H
#define GYRUS_CONNECTION_H

#include <glib-object.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GYRUS_TYPE_CONNECTION            (gyrus_connection_get_type ())
#define GYRUS_CONNECTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GYRUS_TYPE_CONNECTION, GyrusConnection))
#define GYRUS_CONNECTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GYRUS_TYPE_CONNECTION, GyrusConnectionClass))
#define GYRUS_IS_CONNECTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GYRUS_TYPE_CONNECTION))
#define GYRUS_IS_CONNECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GYRUS_TYPE_CONNECTION))
#define GYRUS_CONNECTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GYRUS_TYPE_CONNECTION, GyrusConnection))

typedef enum {
	GYRUS_CONN_FAILED,
	GYRUS_CONN_DONE
} GyrusConnectionStatus;

typedef struct _GyrusConnection GyrusConnection;
typedef struct _GyrusConnectionPrivate GyrusConnectionPrivate;
typedef struct _GyrusConnectionClass GyrusConnectionClass;

struct _GyrusConnection {
	GObject parent;

	GyrusConnectionPrivate *priv;
};

struct _GyrusConnectionClass {
	GObjectClass parent_class;

	/* Signal   : "connect_response"
	 * Prototype: void func (GyrusConnection *conn,
	 *                       GyrusConnectionStatus status,
	 *                       gpointer user_data);
	 */

	/* Signal   : "data_received"
	 * Prototype: void func (GyrusConnection *conn,
	 *                       const gchar *data,
	 *                       gpointer user_data);
	 */
};

GType            gyrus_connection_get_type       (void) G_GNUC_CONST;

GyrusConnection *gyrus_connection_new            (gchar *host,
						  gint port,
						  gboolean usetls);

void             gyrus_connection_destroy        (GyrusConnection *conn);
void             gyrus_connection_connect        (GyrusConnection *conn);
void             gyrus_connection_disconnect     (GyrusConnection *conn);
void             gyrus_connection_send           (GyrusConnection *conn,
                                                  const gchar *buffer);
gboolean         gyrus_connection_get_connected  (GyrusConnection *conn);
G_CONST_RETURN gchar * gyrus_connection_get_host (GyrusConnection *conn);
gint             gyrus_connection_get_port       (GyrusConnection *conn);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GYRUS_CONNECTION_H */
