/*
 gyrus-report.h
 
 GYRUS -- GNOME Cyrus Administrator.
 
 Copyright (C) 2005 Alejandro Valdés <avaldes@utalca.cl>
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 
*/


#ifndef GYRUS_REPORT_H
#define GYRUS_REPORT_H

#include "gyrus-admin.h"

void gyrus_report_show_report (GyrusAdmin *admin);

#endif /* GYRUS_REPORT_H */
