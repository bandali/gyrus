/*
  gyrus-common.h - Common modules.

  GYRUS -- GNOME Cyrus Administrator.

  Copyright (C) 2003-2004 Alejandro Valdes J.
  Copyright (C) 2003-2004 Jorge Bustos B.
  Copyright (C) 2003-2004 Claudio Saavedra V.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#ifndef GYRUS_COMMON_H
#define GYRUS_COMMON_H

void
gyrus_common_show_message (GtkWindow *parent, GtkMessageType type,
			   const gchar* message);
/**
   Returns true if the given #GtkEntry has text. It has special
   care with text composed only of spaces.
*/
gboolean
gyrus_gtk_entry_has_text (GtkEntry *entry);

/**
   Asks the user for a password. Returns a newly allocated string with the
   password, or NULL if the user cancel the login.
*/
gchar *gyrus_dialog_password_new (void);

/**
   Returns TRUE if @str is a valid ASCII string. A
   valid ASCII string is composed of chars under the 
   128 value.
*/
gboolean
gyrus_common_str_is_ascii (const gchar *str);

#endif /* GYRUS_COMMON_H */
